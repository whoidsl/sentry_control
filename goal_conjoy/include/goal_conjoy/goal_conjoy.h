/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_CONJOY_GOAL_CONJOY_H
#define GOAL_CONJOY_GOAL_CONJOY_H

#include "ds_control/joystick_base.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "sentry_msgs/SentryJoystickEnum.h"

namespace goal_conjoy
{
struct GoalConjoyPrivate;

class GoalConjoy : public ds_control::JoystickBase
{
  DS_DECLARE_PRIVATE(GoalConjoy);

public:
  GoalConjoy();

  GoalConjoy(int argc, char** argv, const std::string& name);

  ~GoalConjoy() override;
  DS_DISABLE_COPY(GoalConjoy)

  uint64_t type() const noexcept override
  {
    return sentry_msgs::SentryJoystickEnum::CONJOY;
  }

  void onConjoyData(ds_core_msgs::RawData bytes);

protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupServices() override;
  void setupConnections() override;
  void setupTimers() override;

  void onNavData(const ds_nav_msgs::AggregatedState& nav);

  void checkDeadman(const ros::TimerEvent& evt);

private:
  std::unique_ptr<GoalConjoyPrivate> d_ptr_;
};
}
#endif  // GOAL_JOY_GOAL_JOY_H
