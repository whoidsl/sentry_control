#!/usr/bin/env python

import rospy
import rosnode
import unittest
import subprocess
import os


def spinup_node():
    current_env = os.environ.copy()
    # making a new namespace to prevent us from making a mistake of the test
    # taking place on the robot: unlikely but still technically possible
    current_env["ROS_NAMESPACE"] = "isolated_test_namespace"

    run_command = ["rosrun", "sentry_control", "controller", "joystick"]
    process = subprocess.Popen(run_command, env=current_env)
    process.wait()


def shutdown_node():
    kill_command = ["rosnode", "kill", "/isolated_test_namespace/controller"]
    current_env = os.environ.copy()
    process = subprocess.Popen(kill_command, env=current_env)
    process.wait()


class TestNodeRunning(unittest.TestCase):
    def spinup_node_thread(self):
        self.node_process = None
        self.namespace = "isolated_test_namespace"
        self.node_name = self.namespace + "/controller"

        parameters=None
        if parameters is None:
            parameters = {}
            parameters["active_controller"] = 1
            parameters["active_allocation"] = 1
        rospy.set_param(self.namespace, parameters)
        self.node_process = subprocess.Popen(
            ["python3", "-c", "import test_conjoy; test_conjoy.spinup_node()"]
        )

    def shutdown_node_thread(self):
        if self.node_process is not None:
            self.node_process.kill()
        shutdown_node()

    def check_node_running(self):
        all_nodes = rosnode.get_node_names()
        return self.node_name in all_nodes

    def test_node_isolated_namespace(self):
        self.spinup_node_thread()
        rospy.sleep(2.5)
        master = rospy.get_master()
        _, _, topics = master.getPublishedTopics(self.node_name)
        ns = self.namespace if self.namespace[0] == "/" else "/" + self.namespace
        for topic_name, _ in topics:
            # make sure that all topics published by controller are namespaced
            self.assertTrue(ns == topic_name[0 : len(ns)])
        self.shutdown_node_thread()

    def test_node_starts_if_valid(self):
        self.spinup_node_thread()
        rospy.sleep(2.5)
        self.assertTrue(self.check_node_running())
        self.shutdown_node_thread()

    def test_node_stops_if_invalid(self):
        self.spinup_node_thread()
        rospy.sleep(2.5)
        self.assertFalse(self.check_node_running())
        self.shutdown_node_thread()


if __name__ == "__main__":
    rospy.init_node("test_node_running")
    TestNodeRunning().test_node_isolated_namespace()
    import rostest

    rostest.rosrun("goal_conjoy", "test_node_running", TestNodeRunning)
