/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_conjoy/goal_conjoy.h"
#include "goal_conjoy_private.h"

#include <sentry_msgs/SentryAllocationEnum.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <ds_base/BoolCommand.h>
#include <boost/date_time/posix_time/posix_time_io.hpp>

#include <math.h>

namespace goal_conjoy
{
const double RTOD = 180.0 / M_PI;

GoalConjoy::GoalConjoy() : ds_control::JoystickBase(), d_ptr_(std::unique_ptr<GoalConjoyPrivate>(new GoalConjoyPrivate))
{
}

GoalConjoy::GoalConjoy(int argc, char** argv, const std::string& name)
  : ds_control::JoystickBase(argc, argv, name), d_ptr_(std::unique_ptr<GoalConjoyPrivate>(new GoalConjoyPrivate))
{
}

GoalConjoy::~GoalConjoy() = default;

void GoalConjoy::onConjoyData(ds_core_msgs::RawData bytes)
{
  std::string in(bytes.data.begin(), bytes.data.end());

  int iv[8];
  float av[8];
  char swstring[10];
  int swval[4];
  unsigned num_scanned;
  // CONJOY 80 80 80 80 80 80 80 80 0000
  num_scanned = sscanf(in.c_str(), "CONJOY %x %x %x %x %x %x %x %x %s", iv, iv + 1, iv + 2, iv + 3, iv + 4, iv + 5,
                       iv + 6, iv + 7, swstring);
  if (num_scanned != 9)
  {
    ROS_WARN_STREAM("CONJOY " << in << " could not be parsed");
    return;
  }
  for (int i = 0; i < 8; ++i)
  {
    av[i] = ((float)(iv[i] - 0X80)) / 128.0;
    if (fabs(av[i]) > 1.0)
    {
      ROS_WARN_STREAM("CONJOY arg " << i << " of " << in << " is out of range");
      return;
    }
  }
  for (int i = 0; i < 4; ++i)
  {
    swval[i] = 0;
    if (swstring[i] == '1')
      swval[i] = 1;
  }

  DS_D(GoalConjoy);

  //d->udp_last_msg_ = bytes.ds_header.io_time;
  d->udp_last_msg_ = ros::Time::now();

  // Down is ON
  if (!swval[3])
  {
    if (!enabled())
      setEnabled(true);

    setController(sentry_msgs::SentryControllerEnum::JOYSTICK);
  }
  else if (!enabled())
  {
    ROS_WARN_STREAM("Got CONJOY message, but CONJOY is currently disabled....");
    return;
  }

  ds_nav_msgs::AggregatedState ref{};
  ref.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  ref.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  ref.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;

  if (!enabled())
  {
    // if we get here, the switch is off and some other process
    // has assumed control
    ref.surge_u.value = 0;
    ref.r.value = 0;
    ref.heave_w.value = 0;
  }
  else
  {
    ref.surge_u.value = av[0];
    ref.r.value = av[5];
    ref.heave_w.value = av[2];

    // ROS_ERROR_STREAM("SWSTRING: \"" <<swstring <<"\"");

    if (swval[0] && swval[1])
    {
      // labelled "abort", so go vertical
      setAllocation(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE);
      // ROS_ERROR_STREAM("CONJOY Switches 11, setting VERTICAL");
    }
    else if (swval[0] && !swval[1])
    {
      // labelled "ROV", so go ROV
      setAllocation(sentry_msgs::SentryAllocationEnum::ROV_MODE);
      // ROS_ERROR_STREAM("CONJOY Switches 01, setting ROV");
    }
    else if (!swval[0] && !swval[1])
    {
      // labelled "Flight", so go flight
      setAllocation(sentry_msgs::SentryAllocationEnum::FLIGHT_MODE);
      // ROS_ERROR_STREAM("CONJOY Switches 00, setting FLIGHT");
    }
    else
    {
      // ROS_ERROR_STREAM("CONJOY Switches 10, setting ____");
    }
    // add disable?

    // enable (or not) the auto-heading state
    bool update_auto_heading = d->auto_heading_unknown || (swval[2] != d->auto_heading_state);
    if (update_auto_heading)
    {
      d->auto_heading_state = swval[2];
      ds_base::BoolCommand srv;
      srv.request.command = swval[2];
      ROS_INFO_STREAM("Setting autoheading to " << swval[2]);
      if (d->auto_heading_switch.exists() && d->auto_heading_switch.call(srv))
      {
        d->auto_heading_unknown = false;
      }
      else
      {
        d->auto_heading_unknown = true;
      }
    }
  }
  applyGainsAndPublish(ref);
}

void GoalConjoy::setupSubscriptions()
{
  JoystickBase::setupSubscriptions();

  DS_D(GoalConjoy);
  auto nh = nodeHandle();
  d->agg_nav_ = nh.subscribe(d->agg_nav_topic_, 1, &GoalConjoy::onNavData, this);
}

void GoalConjoy::onNavData(const ds_nav_msgs::AggregatedState& nav)
{
  DS_D(GoalConjoy);
  char buf[255];
  int idx = 0;
  int rc;
  double hdg = 0, pitch = 0, roll = 0;
  double hdg_rate = 0, pitch_rate = 0, roll_rate = 0;

  // check our rate limit
  ros::Time now = nav.header.stamp;
  if (d->gvx_last_sent_.isValid())
  {
    ros::Duration interval = now - d->gvx_last_sent_;
    if (interval.toSec() < d->gvx_interval_)
    {
      return;
    }
  }

  boost::posix_time::ptime now_ptime = nav.header.stamp.toBoost();
  boost::posix_time::time_facet* facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S.%f");
  // Send AUV message to MC
  std::stringstream datestr;
  datestr.imbue(std::locale(datestr.getloc(), facet));
  datestr << now_ptime;

  // read all incoming nav data from the aggregator and handle invalids
  if (nav.heading.valid)
  {
    hdg = nav.heading.value * RTOD;
  }
  if (nav.r.valid)
  {
    hdg_rate = nav.r.value * RTOD;
  }
  if (nav.pitch.valid)
  {
    pitch = nav.pitch.value * RTOD;
  }
  if (nav.q.valid)
  {
    pitch_rate = nav.q.value * RTOD;
  }
  if (nav.roll.valid)
  {
    roll = nav.roll.value * RTOD;
  }
  if (nav.p.valid)
  {
    roll_rate = nav.p.value * RTOD;
  }

  // print a GVX string
  rc = snprintf(buf + idx, 254 - idx, "GVX %s %s %d %.3f %.3f %.3f ", datestr.str().c_str(),
                d->gvx_vehicle_name_.c_str(), d->gvx_sensor_type_, hdg, pitch, roll);
  if (rc < 0 || rc >= 254 - idx)
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << " failed when making GVX string!");
  }
  idx += rc;

  rc = snprintf(buf + idx, 254 - idx, "%.3f %.3f %.3f ", hdg_rate, pitch_rate, roll_rate);
  if (rc < 0 || rc >= 254 - idx)
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << " failed when making GVX string!");
  }
  idx += rc;

  rc = snprintf(buf + idx, 254 - idx, "%.3f 0 \n", nav.header.stamp.toSec());
  if (rc < 0 || rc >= 254 - idx)
  {
    ROS_ERROR_STREAM(ros::this_node::getName() << " failed when making GVX string!");
  }
  idx += rc;

  // Be sure to NULL terminate
  buf[idx] = 0;

  // and send out over the wire
  std::string toSend(buf, idx);
  d->joy_conn_->send(toSend);
  d->gvx_last_sent_ = now;
}

void GoalConjoy::setupServices()
{
  JoystickBase::setupServices();

  auto nh = nodeHandle();

  DS_D(GoalConjoy);
  if (d->auto_heading_service_.empty())
  {
    ROS_ERROR_STREAM("ERROR: Unable to connect to empty autoheading service");
  }
  d->auto_heading_switch = nh.serviceClient<ds_base::BoolCommand>(d->auto_heading_service_);
}

void GoalConjoy::setupTimers()
{
  DS_D(GoalConjoy);
  d->udp_deadman_timer_ = nodeHandle().createTimer(ros::Duration(1), boost::bind(&GoalConjoy::checkDeadman, this, _1));
  d->udp_deadman_timer_.start();
}

void GoalConjoy::setupParameters()
{
  JoystickBase::setupParameters();

  DS_D(GoalConjoy);
  d->auto_heading_service_ = ros::param::param<std::string>("~auto_heading_service", "");
  d->agg_nav_topic_ = ros::param::param<std::string>("~aggregated_nav_topic", "/nav/agg");

  float rate = ros::param::param<float>("~gvx_message_rate_hz", 2.0);
  d->gvx_interval_ = 1.0 / rate;
  d->gvx_vehicle_name_ = ros::param::param<std::string>("~gvx_vehicle_name", "Sentry");
  d->gvx_sensor_type_ = ros::param::param<int>("~gvx_sensor_num", 25);  // default to what sentry used to use
  d->udp_deadman_sec_ = ros::param::param<double>(
      "~udp_deadman_sec", 1);  // Assume "no data in this time" means "take control and zero everything right now"
  if (d->udp_deadman_sec_ > 0)
  {
    ROS_ERROR_STREAM("Setting a deadman timer on UDP messages from the joybox of " << d->udp_deadman_sec_ << " second"
                                                                                                             "s");
  }
  else
  {
    ROS_FATAL("Not using a deadman value (~udp_deadman_sec in goal_conjoy.cpp)! Shutting down.");
    ros::shutdown();
  }
}

void GoalConjoy::setupConnections()
{
  JoystickBase::setupConnections();
  DS_D(GoalConjoy);
  d->joy_conn_ = addConnection("conjoy", boost::bind(&GoalConjoy::onConjoyData, this, _1));
}

void GoalConjoy::checkDeadman(const ros::TimerEvent& evt)
{
  DS_D(GoalConjoy);

  if (d->udp_last_msg_.isZero())
  {
    return;
  }

  if (d->udp_deadman_sec_ < 0)
  {
    return;
  }

  ros::Duration dt = evt.current_real - d->udp_last_msg_;
  if (dt.toSec() >= d->udp_deadman_sec_)
  {
    ROS_ERROR_STREAM("No udp message in " << dt.toSec() << " seconds, taking control");

    if (!enabled())
      setEnabled(true);

    setController(sentry_msgs::SentryControllerEnum::JOYSTICK);
    setAllocation(sentry_msgs::SentryAllocationEnum::IDLE_MODE);

    ds_nav_msgs::AggregatedState ref{};

    ref.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    ref.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    ref.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    ref.surge_u.value = 0;
    ref.r.value = 0;
    ref.heave_w.value = 0;

    applyGainsAndPublish(ref);
  }
}
}
