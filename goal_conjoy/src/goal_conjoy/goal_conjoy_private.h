/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_CONJOY_GOAL_BASE_PRIVATE_H
#define GOAL_CONJOY_GOAL_BASE_PRIVATE_H
#include "goal_conjoy/goal_conjoy.h"

namespace goal_conjoy
{
struct GoalConjoyPrivate
{
  GoalConjoyPrivate() = default;
  ~GoalConjoyPrivate() = default;

  boost::shared_ptr<ds_asio::DsConnection> joy_conn_;

  bool auto_heading_unknown;
  bool auto_heading_state;

  ros::ServiceClient auto_heading_switch;

  ros::Subscriber agg_nav_;
  std::string agg_nav_topic_;

  // name of the service name;
  std::string auto_heading_service_;

  ros::Time gvx_last_sent_;

  double gvx_interval_;
  std::string gvx_vehicle_name_;
  int gvx_sensor_type_;

  double udp_deadman_sec_;
  ros::Time udp_last_msg_;
  ros::Timer udp_deadman_timer_;
};
}
#endif  // GOAL_JOYSTICK_GOAL_BASE_PRIVATE_H
