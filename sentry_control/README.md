#sentry_control: Vehicle control for AUV Sentry

This package provides a modular, extendable set of behavior controllers and references
for the AUV Sentry.  This is the "ros-ified" version of the previous `rov` control code
with *only* Sentry-specific specialization retained.

# Provided Controllers

- **sentry_control::SentryIdleController**      A "do-nothing" controller that sends no actuator commands.
- **sentry_control::SentrySurfaceController**   Sentry's on-surface controller

# Provided Allocations
The scheme of thruster allocations from `rov` has carried over as the translational
logic between "commanded forces" and "actual actuator commands".  This library provides
three allocations derived from a common base:

- **sentry_control::SentryAllocation**  Base-class for allocations
- **sentry_control::SentryFlightAllocation**    The "normal" flight-mode allocation
- **sentry_control::SentryROVAllocation**       Sentry's slow-and-careful allocation
- **sentry_control::SentryVerticalAllocation**  Sentry's fins-vertical allocation.

## Provided References

- **sentry_control::SentryReferenceRFJoystick**  The human-operated joystick reference
  for while sentry is on the surface.
  