/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_IDLE_CONTROLLER_H
#define SENTRY_CONTROL_SENTRY_IDLE_CONTROLLER_H

#include "ds_control/controller_base.h"
#include "sentry_msgs/SentryControllerEnum.h"

namespace sentry_control
{
struct SentryIdleControllerPrivate;

/// @brief The Sentry Idler Controller
///
/// The Idler controller does nothing.  It does not react in
/// any way to new nav or reference inputs.  It's an "inactive"
/// or "idle" state which may be switched into when you don't want
/// any actuators to move.
///
/// To accomplish this, the Idle controller does not actually call
/// the "executeController()" method anywhere.  Even if it did,
/// the method override never calls publishActuatorInputs.
///
/// That said... PLEASE don't publish actuator inputs with this
/// class.. it's not supposed to do anything, remember?
class SentryIdleController : public ds_control::ControllerBase
{
  DS_DECLARE_PRIVATE(SentryIdleController)

public:
  ///@brief ControllerBase constructor
  ///
  /// You must call `ros::init` separately when using this method.
  explicit SentryIdleController();

  /// @brief IdleController constructor
  ///
  /// Calls ros::init(argc, argv, name) for you.
  ///
  /// \param argc
  /// \param argv
  /// \param name
  SentryIdleController(int argc, char* argv[], const std::string& name);
  ~SentryIdleController() override;
  DS_DISABLE_COPY(SentryIdleController);

  /// @brief Does nothing
  void updateState(const ds_nav_msgs::AggregatedState& msg) override
  {
  }

  /// @brief Does nothing
  void updateReference(const ds_nav_msgs::AggregatedState& msg) override
  {
  }

  uint64_t type() const noexcept override
  {
    return sentry_msgs::SentryControllerEnum::NONE;
  }

  /// Returns zero'd wrench forces.
  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
                                               ds_nav_msgs::AggregatedState state, ros::Duration dt) override;

protected:
  /// Does nothing.
  void executeController(ds_nav_msgs::AggregatedState state, ds_nav_msgs::AggregatedState reference,
                         ros::Duration dt) override
  {
  }

  /// no references to setup.
  void setupReferences() override
  {
  }

private:
  std::unique_ptr<SentryIdleControllerPrivate> d_ptr_;
};
}
#endif  // SENTRY_CONTROL_SENTRY_IDLE_CONTROLLER_H
