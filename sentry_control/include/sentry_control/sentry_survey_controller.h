/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_SURVEY_CONTROLLER_H
#define SENTRY_CONTROL_SENTRY_SURVEY_CONTROLLER_H

#include "ds_control_msgs/BottomFollow1D.h"
#include "ds_control_msgs/DepthFollow.h"
#include "ds_control/survey_controller.h"
#include "sentry_msgs/SentryControllerEnum.h"

namespace ds_util
{
class TrapezoidalSmoother;
class WingDynamics;
}

namespace sentry_control
{
struct SentrySurveyControllerPrivate;

class SentrySurveyController : public ds_control::SurveyController
{
  DS_DECLARE_PRIVATE(SentrySurveyController);

public:
  uint64_t type() const noexcept override
  {
    return sentry_msgs::SentryControllerEnum::SURVEY;
  }

  SentrySurveyController();
  SentrySurveyController(int argc, char** argv, const std::string& name);
  ~SentrySurveyController() override;
  DS_DISABLE_COPY(SentrySurveyController);

  /// @brief Set the ROV allocation gain factor.
  ///
  ///
  /// From the original ROV code:
  ///
  ///     DY 2016/08/30 if we are in ROV mode, change the DOF_W gains
  ///     the p_gain is increased by ROV_GAIN_FACTOR, the d_gain
  ///     is increased by the sqrt of the ROV_GAIN_FACTOR. This
  ///     is consistent with keeping the damping ratio the same
  ///
  /// This gain gives ROV mode a little extra (or a little less)
  /// responsiveness while keeping the dampening ratio constant.
  ///
  /// The default value is 1.3
  ///
  /// \param gain
  void setRovGainFactor(double gain);

  /// @brief Get the ROV allocation gain factor
  ///
  /// \return
  double rovGainFactor() const noexcept;

  void setup() override;
  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState state,
                                               ds_nav_msgs::AggregatedState reference, ros::Duration dt) override;
  bool switchAllocation(size_t id) override;

  /// @brief Set the wing dynamics
  ///
  /// The wing dynamics provide simple models for drag and lift depending on
  /// speed and angle-of-attack.
  ///
  /// \param wing_dynamics
  void setWingDynamics(std::unique_ptr<ds_util::WingDynamics> wing_dynamics);

  /// @brief Return a new copy of the wing dynamics object.
  ///
  /// The returned object has the same parameters as the dynamics
  /// model owned by the thruster allocation, but it is a completely
  /// independent copy.  Modifying this object does not change the model
  /// owned by the allocation.
  ///
  /// This inconvenience is intentional to allow subclasses to update
  /// internal parameters when changing the wing dynamics (such as control gains).
  /// \return
  std::unique_ptr<ds_util::WingDynamics> wingDynamics() noexcept;

  /// @brief Set the vehicle forward drag coefficient
  ///
  /// \return
  double vehicleDragCoefficient() const noexcept;

  /// @brief Get the vehicle forward drag coefficient
  ///
  /// \param value
  void setVehicleDragCoefficient(double value);

  /// @brief Estimate the drag for a pair of fins.
  ///
  /// Note:  This method estimates the drag for a *pair* of fins.
  ///
  /// \param angle_of_attack
  /// \param speed
  /// \return
  double estimateControlSurfaceDrag(double angle_of_attack, double speed);

/// @brief Estimate the lift for a pair of fins.
///
/// Note:  This method estimates the drag for a *pair* of fins.
///
/// \param angle_of_attack
/// \param speed
/// \return
#if 0
  double estimateControlSurfaceLift(double angle_of_attack, double speed);
#endif

  /// @brief Estimate the forward drag force for the vehicle.
  ///
  /// \param fwd_angle_of_attack
  /// \param aft_angle_of_attack
  /// \param speed
  /// \return
  double estimateForwardVehicleDrag(double fwd_angle_of_attack, double aft_angle_of_attack, double speed);

size_t activeDepthGoal() const noexcept;

protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupReferences() override;
  void postEnableHook() override;
  void bottomFollowerStateCallback(const ds_control_msgs::BottomFollow1D& msg) override;
  void handleParameterUpdate(const ds_param::ParamConnection::ParamCollection& params);
  void bottomFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg) override;
  void depthFollowerStateCallback(const ds_control_msgs::DepthFollow& msg);
  void depthFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg);

private:
  std::unique_ptr<SentrySurveyControllerPrivate> d_ptr_;
};
}
#endif  // SENTRY_CONTROL_SENTRY_SURVEY_CONTROLLER_H
