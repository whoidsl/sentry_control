/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_ROV_ALLOCATION_H
#define SENTRY_CONTROL_SENTRY_ROV_ALLOCATION_H

#include "sentry_control/sentry_allocation.h"

namespace sentry_control
{
struct SentryRovAllocationPrivate;

class SentryRovAllocation : public SentryAllocation
{
  DS_DECLARE_PRIVATE(SentryRovAllocation)

public:
  explicit SentryRovAllocation();
  SentryRovAllocation(int argc, char* argv[], const std::string& name);
  ~SentryRovAllocation() override;

  DS_DISABLE_COPY(SentryRovAllocation)

  uint64_t type() const noexcept override
  {
    return AllocationType::ROV_MODE;
  }

  ServoParam calculateServoAngles(const geometry_msgs::Wrench& wrench) override;

private:
  std::unique_ptr<SentryRovAllocationPrivate> d_ptr_;
};

auto DEFAULT_ROV_GAINS = SentryAllocation::Gains{.thruster_force_u = { 0, 0, 0.5, 0.5 },
                                                 .thruster_force_w = { -0.5, -0.5, 0, 0 },
                                                 .thruster_torque_w = { 0, 0, 0.5, 0.5 },
                                                 .servo_force_w = { 0, 0 } };
}
#endif  // SENTRY_CONTROL_SENTRY_ROV_ALLOCATION_H
