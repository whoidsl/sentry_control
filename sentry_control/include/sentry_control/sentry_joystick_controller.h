/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_JOYSTICK_CONTROLLER_H
#define SENTRY_CONTROL_SENTRY_JOYSTICK_CONTROLLER_H

#include "ds_control/joystick_controller.h"
#include "sentry_msgs/SentryControllerEnum.h"

namespace sentry_control
{
struct SentryJoystickControllerPrivate;

class SentryJoystickController : public ds_control::JoystickController
{
  DS_DECLARE_PRIVATE(SentryJoystickController)

public:
  SentryJoystickController();
  SentryJoystickController(int argc, char** argv, const std::string& name);
  ~SentryJoystickController() override;

  uint64_t type() const noexcept override
  {
    return sentry_msgs::SentryControllerEnum::JOYSTICK;
  }

  void setup() override;
  void updateReference(const ds_nav_msgs::AggregatedState& msg) override;

protected:
  void setupParameters() override;

private:
  std::unique_ptr<SentryJoystickControllerPrivate> d_ptr_;
};
}
#endif  // SENTRY_CONTROL_SENTRY_JOYSTICK_CONTROLLER_H
