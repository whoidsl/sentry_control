/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_IDLE_ALLOCATION_H
#define SENTRY_CONTROL_SENTRY_IDLE_ALLOCATION_H

#include "sentry_control/sentry_allocation.h"

namespace sentry_control
{
struct SentryIdleAllocationPrivate;

///@brief Sentry's Idle allocation
///
/// The idle allocation attempts NOT to move any actuators, regardless
/// of the incomming force commands.
///
/// To accomplish this it actively sends 0-current commands to thrusters,
/// and NO commands to servos.
class SentryIdleAllocation : public SentryAllocation
{
  DS_DECLARE_PRIVATE(SentryIdleAllocation)

public:
  explicit SentryIdleAllocation();
  SentryIdleAllocation(int argc, char* argv[], const std::string& name);
  ~SentryIdleAllocation() override;

  DS_DISABLE_COPY(SentryIdleAllocation)

  uint64_t type() const noexcept override
  {
    return AllocationType::IDLE_MODE;
  }

  ///@brief Publish override
  ///
  /// The Idle allocation attempts NOT to move any actuators.  To do this it:
  ///
  /// - Publishes thruster commands of 0 current
  /// - Does NOT publish servo commands.
  ///
  /// \param body_forces
  void publishActuatorCommands(geometry_msgs::WrenchStamped body_forces) override;

protected:
  void setupTimers() override;

private:
  std::unique_ptr<SentryIdleAllocationPrivate> d_ptr_;
};
}
#endif  // SENTRY_CONTROL_SENTRY_IDLE_ALLOCATION_H
