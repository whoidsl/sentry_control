/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_ALLOCATION_H
#define SENTRY_CONTROL_SENTRY_ALLOCATION_H

#include "ds_base/ds_global.h"
#include "ds_param/ds_param_conn.h"
#include "ds_control/allocation_base.h"

#include "ds_nav_msgs/AggregatedState.h"
#include "sentry_msgs/SentryAllocationEnum.h"

#include <geometry_msgs/Wrench.h>
#include <ros/node_handle.h>
#include <memory>

namespace ds_actuator_msgs
{
ROS_DECLARE_MESSAGE(ThrusterCmd);
ROS_DECLARE_MESSAGE(ServoCmd);
}

namespace sentry_control
{
struct SentryAllocationPrivate;

class SentryAllocation : public ds_control::AllocationBase
{
  DS_DECLARE_PRIVATE(SentryAllocation)

public:
  enum ThrusterParamIndex
  {
    PARAM_THRUSTER_PORT_FWD = 0,
    PARAM_THRUSTER_STBD_FWD,
    PARAM_THRUSTER_PORT_AFT,
    PARAM_THRUSTER_STBD_AFT,
    NUM_THRUSTERS,
  };

  enum ServoParamIndex
  {
    PARAM_SERVO_FWD = 0,
    PARAM_SERVO_AFT,
    NUM_SERVOS,
  };

  using ThrusterParam = std::array<double, NUM_THRUSTERS>;
  using ServoParam = std::array<double, NUM_SERVOS>;

  enum AllocationType
  {
    IDLE_MODE = sentry_msgs::SentryAllocationEnum::IDLE_MODE,
    FLIGHT_MODE = sentry_msgs::SentryAllocationEnum::FLIGHT_MODE,
    ROV_MODE = sentry_msgs::SentryAllocationEnum::ROV_MODE,
    VERTICAL_MODE = sentry_msgs::SentryAllocationEnum::VERTICAL_MODE,
    USER_DEFINED = sentry_msgs::SentryAllocationEnum::USER_DEFINED,
  };

  struct Gains
  {
    SentryAllocation::ThrusterParam thruster_force_u;
    SentryAllocation::ThrusterParam thruster_force_w;
    SentryAllocation::ThrusterParam thruster_torque_w;
    SentryAllocation::ServoParam servo_force_w;
  };

  /// @brief AllocationBase constructor
  ///
  /// Calls ros::init(argc, argv, name) for you.
  ///
  /// \param argc
  /// \param argv
  /// \param name
  SentryAllocation(int argc, char* argv[], const std::string& name);
  explicit SentryAllocation();
  explicit SentryAllocation(const Gains& gains);

  ~SentryAllocation() override;
  DS_DISABLE_COPY(SentryAllocation)

  uint64_t type() const noexcept override
  {
    return AllocationType::FLIGHT_MODE;
  }
  /// @brief Maximum "safe" velocity used for invalid references.
  ///
  /// The thrusters require a velocity reference to convert from desired
  /// newtons to commanded current.  However, this transform becomes unstable
  /// as the velocity approaches the limit: alpha/beta, where alpha and beta
  /// are the current transform coefficients.
  ///
  /// For the default values of alpha and beta, this velocity limit is:
  /// 16.25/8.93 = 1.81 (m/s)
  ///
  /// The default max velocity is set to 1.5 to provide some extra margin.
  ///
  /// \param speed
  void setMaxSpeed(double speed);

  /// @brief Get the maximum "safe" velocity used for invalid references.
  ///
  /// \return
  double maxSpeed() const noexcept;

  /// @brief Set the maximum allowed commanded thrust (in newtons)
  ///
  /// \param newtons
  virtual void setMaxThrusterForce(double newtons);

  /// @brief Get the maximum allowed commanded thrust (in newtons)
  ///
  /// \return
  double maxThrusterForce() const noexcept;

  /// @brief Set the maximum allowed commanded current (in amps)
  ///
  /// \param newtons
  void setMaxThrusterCurrent(double amps);

  /// @brief Get the maximum allowed commanded current (in amps)
  ///
  /// \return
  double maxThrusterCurrent() const noexcept;

  /// @brief Set the TTL for all thruster commands (in seconds)
  ///
  /// param seconds
  void setThrusterTTL(double seconds);

  /// @brief Get the TTL for all thruster commands (in seconds)
  ///
  double thrusterTTL() const noexcept;

  /// @brief Set the maximum allowed commanded servo angle (in radians)
  ///
  /// \param radians
  virtual void setMaxServoAngle(double radians);

  /// @brief Get the maximum allowed commanded servo angle (in radians)
  ///
  /// \return
  double maxServoAngle() const noexcept;

  /// @brief Set the current body-frame speed of the thruster
  ///
  /// The body-frame speed is required to convert from desired thrust
  /// to commanded current.
  ///
  /// The maximum allowed velocity (set by `setMaxSpeed()`) is the hard
  /// upper limit.  Providing a velocity here greater than this limit will
  /// saturate at the maximum velocity value.
  ///
  /// \param speed
  void setReferenceSpeed(double speed);

  /// @brief Get the desired forward reference velocity.
  ///
  /// \return
  double referenceSpeed() const noexcept;

  void setWingLiftCoefficient(double newtons);

  double wingLiftCoefficient() const noexcept;

  /// @brief Change the force-to-current transform coeficients
  ///
  /// The current required to produce the desired force is calculated
  /// by the following formula:
  ///
  /// I = F / (alpha + beta * speed)
  ///
  /// where
  ///   - I: is the required current
  ///   - F: is the desired force
  ///   - speed:  is the vehicle speed (non-negative!)
  ///   - alpha, beta: emperically derived fitting coeficients.
  ///
  /// The default values are:
  ///    - alpha: -16.25
  ///    - beta: 8.93
  ///
  /// This creates instability as vel approaches alpha/beta = 1.81 (m/s)
  ///
  /// \param alpha
  /// \param beta
  void setCurrentCoefficients(double alpha, double beta);

  /// @brief Get the force-to-current transform coefficients
  ///
  /// \return the pair: (alpha, beta)
  std::pair<double, double> currentCoefficients() const noexcept;

  /// @brief Calculate the required current to produce the desired force
  ///
  /// The current is calculated using:
  ///
  /// I = F / (alpha + beta * speed)
  ///
  /// where
  ///   - I: is the required current
  ///   - F: is the desired force
  ///   - speed:  is the vehicle speed (non-negative!)
  ///   - alpha, beta: emperically derived fitting coeficients.
  ///
  /// The default values are:
  ///    - alpha: -16.25
  ///    - beta: 8.93
  ///
  /// This creates instability as vel approaches alpha/beta = 1.81 (m/s)
  ///
  /// \param force
  /// \param speed
  /// \return
  double currentFromForce(double force, double speed) const noexcept;

  /// @brief Transform from thruster current to approximate force
  ///
  /// This method is the inverse of currentFromForce
  ///
  /// \param current
  /// \param fwd_speed
  /// \return
  double forceFromCurrent(double current, double fwd_speed) const noexcept;

  /// @brief Calculate required thruster forces
  ///
  /// This method returns the desired force produced by each thruster for the overall
  /// body-frame wrench force.
  ///
  /// Different allocations may spread the total forces differently amongst Sentry's
  /// thrusters.
  ///
  /// \param wrench
  /// \return

  virtual ThrusterParam calculateThrusterForces(const geometry_msgs::Wrench& wrench);

  /// @brief Calculate the required servo angles
  ///
  /// This method returns the desired servo angles for the overall body-frame wrench force.
  ///
  /// \param wrench
  /// \return
  virtual ServoParam calculateServoAngles(const geometry_msgs::Wrench& wrench);

  /// @brief Publish thruster commands
  ///
  /// Send commands to sentry's thrusters.
  ///
  /// \param commands
  void publishThrusterCommands(const std::array<ds_actuator_msgs::ThrusterCmd, NUM_THRUSTERS>& commands);

  /// @brief Publish servo commands
  ///
  /// Send commands to sentry's servos
  /// \param commands
  void publishServoCommands(const std::array<ds_actuator_msgs::ServoCmd, NUM_SERVOS>& commands);

  /// @brief Publish actuator commands for the desired force.
  ///
  /// This is the main callback for Sentry's allocations.  It will:
  ///
  /// - Clamp the forces to the desired range
  /// - Calculate thruster forces with calculateThrusterForces()
  /// - Calculate thruster currents with currentFromForce
  /// - Invert thruster currents if required
  /// - Publish thruster commands with publishThrusterCommands()
  /// - Calculate servo angles with calculateServoAngles()
  /// - Publish servo commands with publishServoCommands()
  ///
  /// \param body_forces
  void publishActuatorCommands(geometry_msgs::WrenchStamped body_forces) override;

  /// @brief Update the allocation with new reference state
  ///
  /// All allocations need to know a current "speed" estimate to calculate
  /// the required thruster currents from a desired body force.
  ///
  /// The default way of getting this speed is to look at the incoming reference
  /// messages.  The "surge_u" value will be used if it is valid, otherwise the
  /// speed set by `setMaxSpeed()` will be used.
  ///
  /// \param ref_msg
  virtual void referenceUpdate(const ds_nav_msgs::AggregatedState& ref_msg) override;

  /// @brief Read actuator parameters from the robot description.
  ///
  /// If successful, this method will set the following from the robot description:
  ///   - thruster moment arms.
  ///
  /// \param param
  /// \return
  bool readRobotDescription(const std::string& param = "/sentry/robot_description");

  /// @brief Set the thruster's moment arms on the heading axis
  ///
  /// The total heading torque gain for each thruster is:
  ///
  ///    heading_gain * moment_arm
  ///
  /// \param moment_arms
  virtual void setMomentArms(ThrusterParam arms);

  /// @brief Get the moment arms on the heading axis for the thrusters.
  ///
  /// \return
  ThrusterParam momentArms() const noexcept;

#if 0
  /// @brief Use settings on the parameter server
  ///
  /// This method looks for the following settings:
  ///
  /// -`dynamics_ns`:  A string with the namespace for the following parameters.  Default is empty
  ///   which will use the current node's namespace.
  ///
  /// - `thruster_max_force`
  /// - `thruster_max_current`
  /// - `force_to_amps_transform/alpha`:  used in the force/current transform
  /// - `force_to_amps_transform/beta`: used in the force/current transform
  /// - `pos_force_is_pos_current/thruster_fwd_port`
  /// - `pos_force_is_pos_current/thruster_fwd_stbd`
  /// - `pos_force_is_pos_current/thruster_aft_port`
  /// - `pos_force_is_pos_current/thruster_aft_stbd`
  ///    Used to switch the rotation direction of thrusters for counter-rotating props.
  /// - `CS_Area`    control surface area
  /// - `CS_Rho`     water density
  /// - `CS_Cl0`     lift coefficient
  /// - `CS_Cl1`     lift coefficient
  /// - `CS_Cl2`     lift coefficient
  /// - `CS_alpha1`  lift coefficient
  /// - `CS_alpha2`  lift coefficient
  /// - `CS_Cd0`     drag coefficient
  /// - `CS_Cd1`     drag coefficient
  /// - `CS_Cd2`     drag coefficient
  /// -  `/robot_description`:  Used to calculate the heading moment arms.
  /// \param nh
  virtual void setupFromParameterServer(ros::NodeHandle& nh);
#endif

  /// @brief Balance a pair of thrusters between forward and yaw thrusts
  ///
  /// This is the old "fix_saturation" function from feedback.cpp:
  ///
  ///    fix_saturation
  ///    2006/04/23 DY
  ///    deal with saturation of aft thrusters due to combination of fwd and yaw
  ///    thrust subtracts from both to keep the largest one at the limit
  ///    does nothing for backing up
  ///    // 2014-04-14 SS, DRY modified so that it could work with negative numbers
  ///
  /// This method modifies the thrust on the aft thrusters after the desired thrusts
  /// are calculated, but before commands are sent to the servos.
  ///
  /// \param port
  /// \param stbd
  void balanceSteeringThrusters(double& port, double& stbd);

  /// @brief Sets the wrench.force.x gains on the thrusters
  ///
  /// \param gains
  virtual void setForwardGain(ThrusterParam gains);

  /// @brief Get the forward force gains for the thrusters.
  ThrusterParam forwardGain() const noexcept;

  /// @brief Sets the wrench.force.x gains on the thrusters
  ///
  /// \param gains
  virtual void setVerticalGain(ThrusterParam gains);

  /// @brief Get the forward force gains for the thrusters.
  ThrusterParam verticalGain() const noexcept;

  /// @brief Sets the wrench.torque.z gains on the thrusters
  ///
  /// \param gains
  virtual void setHeadingGain(ThrusterParam gains);

  /// @brief Get the heading torque gains for the thrusters.
  ThrusterParam headingGain() const noexcept;

  /// @brief Sets the wrench.force.z gains on the thrusters.
  ///
  /// \param gains
  virtual void setServoGain(ServoParam gains);

  /// @brief Get the servo gains
  ///
  /// \return
  ServoParam servoGain() const noexcept;

  /// @brief Set the allocation gains using a defined set.
  ///
  /// \param gains
  virtual void setGains(Gains gains);

  /// @brief Get the gains for the allocation.
  ///
  /// \return
  Gains gains() const noexcept;

  /// @brief Get the wrench gains for thrusters
  ///
  /// The wrench gains are the final gains applied to incoming body-force wrenches.
  /// For example, the gain applied to the torque.z axis is a thruster's heading gain
  /// multiplied by it's moment arm:
  ///
  ///   torque.z = heading_gain * moment_arm/setup
  const std::array<geometry_msgs::Wrench, NUM_THRUSTERS>& thrusterWrenchGains() const noexcept;

  /// @brief Get the wrench gains for servos
  ///
  /// The wrench gains are the final gains applied to incoming body-force wrenches.
  /// For example, the gain applied to the force.z axis is a servo's gain divided by
  /// the wing lift at 1/ms
  ///
  ///   force.z = servo_gain / wing_lift
  const std::array<geometry_msgs::Wrench, NUM_SERVOS>& servoWrenchGains() const noexcept;

  void setPosForceIsPosCurrent(std::array<bool, NUM_THRUSTERS> currents);
  std::array<bool, NUM_THRUSTERS> posForceIsPosCurrent() const noexcept;

protected:
  void setupParameters() override;
  void setupPublishers() override;
  void setupSubscriptions() override;

  virtual void parameterSubscriptionCallback(const ds_param::ParamConnection::ParamCollection& params)
  {
  }

private:
  std::unique_ptr<SentryAllocationPrivate> d_ptr_;
};

std::string to_string(SentryAllocation::ThrusterParamIndex index);
std::ostream& operator<<(std::ostream& os, const SentryAllocation::ThrusterParamIndex& index);

std::string to_string(SentryAllocation::ServoParamIndex index);
std::ostream& operator<<(std::ostream& os, const SentryAllocation::ServoParamIndex& index);

// NOTE: The original ROV uses 0.76 as the magnitude for these values, which is ~30 inches.  The actual value is
// closer to 29 inches, or 0.7365m
static const auto DEFAULT_SENTRY_MOMENT_ARMS = SentryAllocation::ThrusterParam{ 0.7365, -0.7365, 0.7365, -0.7365 };
const auto FLIGHT0_ALLOCATION_GAINS = SentryAllocation::Gains{.thruster_force_u = { 0, 0, 0.5, 0.5 },
                                                              .thruster_force_w = { 0, 0, 0.0, 0.0 },
                                                              .thruster_torque_w = { 0, 0, 0.5, 0.5 },
                                                              .servo_force_w = { -0.5, -0.5 } };

const auto FLIGHT1_ALLOCATION_GAINS = SentryAllocation::Gains{.thruster_force_u = { 0.3, 0.3, 0.2, 0.2 },
                                                              .thruster_force_w = { 0, 0, 0.0, 0.0 },
                                                              .thruster_torque_w = { 0, 0, 0.5, 0.5 },
                                                              .servo_force_w = { -0.5, -0.5 } };

}  // namespace sentry_control

#endif  // SENTRY_CONTROL_SENTRY_ALLOCATION_H
