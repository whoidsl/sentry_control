/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_BALLAST_CONTROLLER_H
#define SENTRY_CONTROL_SENTRY_BALLAST_CONTROLLER_H

#include "sentry_msgs/SentryControllerEnum.h"
#include "sentry_msgs/InitiateBallastTest.h"
#include "ds_control/controller_base.h"

namespace sentry_control
{
struct SentryBallastControllerPrivate;

/// @brief A ballast test controller
///
/// Ballast tests can be conducted in either constant-altitude or constant-depth modes.  This
/// controller is intended to be started via the `~initiate_ballast_test` service, and there are
/// hook in the `sentry_mc` MC shim node to call tests for the old `BAL`, `BALA`, and `BALZ` MC
/// commands.
class SentryBallastController : public ds_control::ControllerBase
{
  DS_DECLARE_PRIVATE(SentryBallastController)

public:
  SentryBallastController();
  SentryBallastController(int argc, char* argv[], const std::string& name);
  ~SentryBallastController();
  DS_DISABLE_COPY(SentryBallastController)

  uint64_t type() const noexcept override
  {
    return sentry_msgs::SentryControllerEnum::BALLAST;
  }

  void updateState(const ds_nav_msgs::AggregatedState& msg) override;
  geometry_msgs::WrenchStamped calculateForces(ds_nav_msgs::AggregatedState reference,
                                               ds_nav_msgs::AggregatedState state, ros::Duration dt) override;

  /// @brief Initiate a Ballast Test
  ///
  /// Ballast tests have two parameters:
  ///
  ///   `hold_altitude` (bool)  Hold altitude, or hold depth
  ///   `ballast_testpoint` (float)
  ///
  /// ## Constant Altitude
  /// When `hold_altitude` is true the ballast test will be run the altitude given by the `ballast_testpoint` value.
  /// Values <= 10 are ignored and will default to a 80m altitude test.
  ///
  /// ## Constant Depth
  /// When `hold_altitude` is false the ballast test will be run at a constant depth.  The depth value is provided by
  /// `ballast_testpoint`:
  ///
  ///  `ballast_testpoint` >  0:  Hold the provided depth.
  ///  `ballast_testpoint` <= 0:  Hold the current depth.
  ///
  /// \param req
  /// \param res
  /// \return
  bool initiateBallastTest(const sentry_msgs::InitiateBallastTest::Request& req, sentry_msgs::InitiateBallastTest::Response& res);

  /// @brief Reset the controller to an uninitialzed state
  void reset() override;

protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupReferences() override;
  void setupServices() override;

  /// @brief pre-disable hook
  ///
  /// This hook restores the bottom follower altitude if the controller
  /// is in "hold-altitude" mode.
  void preDisableHook() override;

  void bottomFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg);

private:
  std::unique_ptr<SentryBallastControllerPrivate> d_ptr_;
};
}
#endif  // PROJECT_SENTRY_BALLAST_CONTROLLER_H
