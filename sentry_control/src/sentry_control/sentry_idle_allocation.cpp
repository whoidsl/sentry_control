/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include <ds_actuator_msgs/ThrusterCmd.h>
#include "sentry_control/sentry_idle_allocation.h"

namespace sentry_control
{
struct SentryIdleAllocationPrivate
{
  ros::Timer command_timer_;
};

SentryIdleAllocation::SentryIdleAllocation()
  : SentryAllocation(), d_ptr_(std::unique_ptr<SentryIdleAllocationPrivate>(new SentryIdleAllocationPrivate))
{
}

SentryIdleAllocation::SentryIdleAllocation(int argc, char* argv[], const std::string& name)
  : SentryAllocation(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryIdleAllocationPrivate>(new SentryIdleAllocationPrivate))
{
}

SentryIdleAllocation::~SentryIdleAllocation() = default;

void SentryIdleAllocation::publishActuatorCommands(geometry_msgs::WrenchStamped body_forces)
{
  if (!enabled())
  {
    return;
  }

  const auto now = ros::Time::now();
  auto thruster_commands = std::array<ds_actuator_msgs::ThrusterCmd, NUM_THRUSTERS>{};
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    auto& cmd = thruster_commands.at(i);
    cmd.stamp = now;
    cmd.cmd_newtons = 0;
    cmd.cmd_value = 0;
    cmd.ttl_seconds = 0;  // Immediately timeout and put the thruster in a deadman state
    cmd.thruster_name = to_string(static_cast<ThrusterParamIndex>(i));
  }

  publishThrusterCommands(thruster_commands);
}
void SentryIdleAllocation::setupTimers()
{
  DsProcess::setupTimers();
  DS_D(SentryIdleAllocation);

  auto callback = [](const ros::TimerEvent&, SentryIdleAllocation* base) {
    static auto forces = geometry_msgs::WrenchStamped{};
    base->publishActuatorCommands(forces);
  };

  d->command_timer_ = nodeHandle().createTimer(ros::Duration(0.1), boost::bind<void>(callback, _1, this));
}
}
