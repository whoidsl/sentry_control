/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_control/pid.h"
#include "sentry_msgs/SentryAllocationEnum.h"
#include "sentry_control/sentry_ballast_controller.h"

namespace sentry_control
{
struct SentryBallastControllerPrivate
{
  ds_param::DoubleParam::Ptr bottom_follower_altitude_sub_;
  float original_bf_altitude_ = 65;
  bool restore_bf_altitude_ = false;

  bool hold_altitude_ = false;
  float ballast_testpoint_ = 0;

  ds_control::Pid pid_down_;
  ros::Subscriber bottom_follower_ref_sub_;
  ds_nav_msgs::AggregatedState last_bottom_follower_ref_;
  ros::ServiceServer ballast_test_init_srv_;
};

SentryBallastController::SentryBallastController()
  : ControllerBase(), d_ptr_(std::unique_ptr<SentryBallastControllerPrivate>(new SentryBallastControllerPrivate))
{
}

SentryBallastController::SentryBallastController(int argc, char** argv, const std::string& name)
  : ControllerBase(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryBallastControllerPrivate>(new SentryBallastControllerPrivate))
{
}

void SentryBallastController::setupSubscriptions()
{
  ControllerBase::setupSubscriptions();
  auto param_sub = parameterSubscription();

  auto param_name = std::string{ "~bottom_follower_altitude_param" };
  auto param_value_str = std::string{};
  if (!ros::param::get(param_name, param_value_str))
  {
    ROS_FATAL_STREAM("Missing parameter named " << param_name);
    ROS_BREAK();
  }
  DS_D(SentryBallastController);
  d->bottom_follower_altitude_sub_ = param_sub->connect<ds_param::DoubleParam>(param_value_str);
}

void SentryBallastController::preDisableHook()
{
  ControllerBase::preDisableHook();

  DS_D(SentryBallastController);
  if (d->restore_bf_altitude_)
  {
    if (d->original_bf_altitude_ > 0)
    {
      d->bottom_follower_altitude_sub_->Set(d->original_bf_altitude_);
    }
    else
    {
      ROS_ERROR_STREAM("Attempted to restore invalid bottom follower altitude of " << d->original_bf_altitude_);
      ROS_ERROR_STREAM("Leaving bottom follower altitude at " << d->bottom_follower_altitude_sub_->Get());
    }

    d->restore_bf_altitude_ = false;
  }

  // Clear the reference
  auto ref = referenceRef();
  ref.down.valid = false;
  ref.header.stamp = ros::Time::now();
}

void SentryBallastController::setupReferences()
{
  const auto& refs = referenceMap();

  auto it = refs.find("bottom_follower");
  if (it == std::end(refs))
  {
    ROS_FATAL("No 'bottom_follower' reference provided for survey controller");
    ROS_BREAK();
  }
  auto nh = nodeHandle();
  auto topic = ros::names::resolve(it->second, std::string{ "goal_state" });
  DS_D(SentryBallastController);
  d->bottom_follower_ref_sub_ = nh.subscribe(topic, 1, &SentryBallastController::bottomFollowerGoalCallback, this);
}

void SentryBallastController::bottomFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg)
{
  if (!msg.header.stamp.isValid())
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "Bottom Follower referenced provided an invalid timestamp");
    return;
  }

  if (!msg.down.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "Bottom Follower reference provided an invalid depth");
    return;
  }

  DS_D(SentryBallastController);
  d->last_bottom_follower_ref_ = msg;

  if (!enabled())
  {
    return;
  }

  if (d->hold_altitude_)
  {
    auto ref = reference();
    ref.header = msg.header;

    ref.down.valid = true;
    ref.down.value = msg.down.value;

    updateReference(ref);
  }
}
void SentryBallastController::updateState(const ds_nav_msgs::AggregatedState& msg)
{
  const auto last_state = state();
  ControllerBase::updateState(msg);

  if (!enabled())
  {
    return;
  }

  const auto dt = msg.header.stamp - last_state.header.stamp;

  if (dt.toSec() < 0.01)
  {
    ROS_ERROR_STREAM("Skipping controller execution due to small dt: " << dt);
    return;
  }
  else if (dt.toSec() > 10)
  {
    ROS_ERROR_STREAM("Skipping controller executing due to large dt: " << dt);
    return;
  }

  const auto ref = reference();
  if (!ref.header.stamp.isValid())
  {
    ROS_ERROR_STREAM("Skipping controller execution, reference timestamp is invalid.");
    return;
  }

  // Update the timestamp of our constant-depth reference (makes plotting easier)
  DS_D(SentryBallastController);
  if (!d->hold_altitude_)
  {
    auto ref = reference();
    ref.header.stamp = ros::Time::now();
    updateReference(ref);
  }
  // Execute the control loop when enabled
  executeController(ref, msg, std::move(dt));
}

geometry_msgs::WrenchStamped SentryBallastController::calculateForces(ds_nav_msgs::AggregatedState reference,
                                                                      ds_nav_msgs::AggregatedState state,
                                                                      ros::Duration dt)
{
  auto wrench = geometry_msgs::WrenchStamped{};
  wrench.header.stamp = ros::Time::now();
  wrench.header.frame_id = wrenchFrameId();

  const auto dt_ = dt.toSec();
  DS_D(SentryBallastController);
  if (reference.down.valid && state.down.valid)
  {
    wrench.wrench.force.z = d->pid_down_.calculate_with_measured_rate(
        state.down.value, reference.down.value, state.heave_w.value, reference.heave_w.value, dt_);
  }
  else if (!reference.down.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No W force produced: Invalid reference down.");
  }
  else if (!state.down.valid)
  {
    ROS_ERROR_STREAM_THROTTLE(0.5, "No W force produced: Invalid state down.");
  }

  return wrench;
}
void SentryBallastController::setupParameters()
{
  ControllerBase::setupParameters();
  DS_D(SentryBallastController);

  const auto dynamics_ns = ros::param::param<std::string>("dynamics_ns", "");
  auto param_name = ros::names::resolve(dynamics_ns, std::string{ "max_thruster_force" });
  const auto max_thrust = ros::param::param<double>(param_name, 80.0);

  d->pid_down_.setupFromParameterServer("~down_pid");
  d->pid_down_.setMaxSaturation(6 * max_thrust);
  d->pid_down_.setMinSaturation(-6 * max_thrust);
  d->pid_down_.setMaxIntegratedError(0.2 * max_thrust / d->pid_down_.ki());
  d->pid_down_.setMinIntegratedError(-0.2 * max_thrust / d->pid_down_.ki());
}
void SentryBallastController::setupServices()
{
  DsProcess::setupServices();

  DS_D(SentryBallastController);

  d->ballast_test_init_srv_ = nodeHandle()
                                  .advertiseService<sentry_msgs::InitiateBallastTest::Request,
                                                    sentry_msgs::InitiateBallastTest::Response>(
                                      ros::this_node::getName() + "/initiate_ballast_test",
                                      boost::bind(&SentryBallastController::initiateBallastTest, this, _1, _2));
}

bool SentryBallastController::initiateBallastTest(const sentry_msgs::InitiateBallastTest::Request& req,
                                                  sentry_msgs::InitiateBallastTest::Response& res)
{
  // Reset internals
  auto last_state = state();
  reset();

  DS_D(SentryBallastController);
  d->original_bf_altitude_ = d->bottom_follower_altitude_sub_->Get();
  d->restore_bf_altitude_ = false;

  auto testpoint = req.ballast_testpoint;
  auto hold_altitude = static_cast<bool>(req.hold_altitude);

  if (req.hold_altitude && testpoint <= 10)
  {
    ROS_ERROR_STREAM("Attempting to run ballast controller at constant altitude <= 10m (" << d->ballast_testpoint_
                                                                                          << ")");
    ROS_ERROR_STREAM("Using a safe value of 80m instead.");
    testpoint = 80;
  }

  if (!req.hold_altitude && testpoint <= 0)
  {
    if (!last_state.down.valid)
    {
      ROS_ERROR_STREAM("Invalid nav depth value at ballast test execution, falling back to 80m altitude instead");
      hold_altitude = true;
      testpoint = 80;
    }
    else
    {
      testpoint = static_cast<float>(last_state.down.value);
    }
  }

  d->hold_altitude_ = hold_altitude;
  d->ballast_testpoint_ = testpoint;

  if (d->hold_altitude_)
  {
    // Restore the bottom follower altitude when completed
    d->restore_bf_altitude_ = true;
    // Set our new altitude goal
    d->bottom_follower_altitude_sub_->Set(d->ballast_testpoint_);

    // Switch in the last bottom follower reference message as our last reference.
    updateReference(d->last_bottom_follower_ref_);
  }
  else
  {
    auto ref = reference();
    ref.header.stamp = ros::Time::now();
    ref.down.valid = true;
    ref.down.value = d->ballast_testpoint_;
    updateReference(ref);
  }

  // Set allocation into Vertical
  if (!switchAllocation(sentry_msgs::SentryAllocationEnum::VERTICAL_MODE))
  {
    ROS_ERROR_STREAM("Failed to switch into vertical mode at end of post-enable hook!");
    ROS_ERROR_STREAM("Still in allocation: " << activeAllocation());
  }

  setEnabled(true);
  return true;
}
void SentryBallastController::reset()
{
  ControllerBase::reset();
  DS_D(SentryBallastController);
  d->pid_down_.reset();
}
SentryBallastController::~SentryBallastController() = default;
}
