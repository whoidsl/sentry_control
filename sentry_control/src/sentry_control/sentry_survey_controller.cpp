/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_survey_controller.h"
#include "sentry_control/sentry_allocation.h"
// #include "sentry_control/sentry_rov_allocation.h"
// #include "sentry_control/sentry_vertical_allocation.h"

#include "ds_control/pid.h"

#include "ds_util/dynamics.h"
#include "ds_util/reference_smoothing.h"

#include "sentry_survey_controller_private.h"

namespace sentry_control
{
SentrySurveyController::SentrySurveyController()
  : SurveyController(), d_ptr_(std::unique_ptr<SentrySurveyControllerPrivate>(new SentrySurveyControllerPrivate))
{
}

SentrySurveyController::SentrySurveyController(int argc, char** argv, const std::string& name)
  : SurveyController(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentrySurveyControllerPrivate>(new SentrySurveyControllerPrivate))
{
}

SentrySurveyController::~SentrySurveyController() = default;

void SentrySurveyController::setup()
{
  SurveyController::setup();
  setWrenchFrameId("base_link_NED");

  // auto nh = nodeHandle();

  // DS_D(SentrySurveyController);
  // if(!switchAllocation(SentryAllocation::AllocationType ::FLIGHT_MODE))
  // {
  //   ROS_FATAL("Unable to switch to FLIGHT allocation.");
  //   ROS_BREAK();
  // }
}

void SentrySurveyController::setupParameters()
{
  SurveyController::setupParameters();
  DS_D(SentrySurveyController);

  auto nh = nodeHandle();
  d->param_sub_ = ds_param::ParamConnection::create(nh);
  d->param_active_depth_goal_ = d->param_sub_->connect<ds_param::IntParam>("active_depth_goal");
  d->param_sub_->setCallback(boost::bind(&SentrySurveyController::handleParameterUpdate, this, _1));

  const auto dynamics_ns = ros::param::param<std::string>("dynamics_ns", "");
  auto param_name = ros::names::resolve(dynamics_ns, std::string{ "max_thruster_force" });

  const auto max_thrust = ros::param::param<double>(param_name, 80.0);
  auto pid = pidHeading();

  // 0.76 is a rough approximation of the moment arm from a thruster to the centerline of
  // Sentry.  The actual arm length given by the ROS model is more like 0.73, but 0.76 was
  // used in the original ROV code.
  pid->setMaxSaturation(2 * max_thrust / 0.76);
  pid->setMinSaturation(-2 * max_thrust / 0.76);

  pid = pidSurgeU();
  pid->setMaxSaturation(4 * max_thrust);
  pid->setMinSaturation(-4 * max_thrust);

  pid = pidDown();
  pid->setMaxSaturation(6 * max_thrust);
  pid->setMinSaturation(-6 * max_thrust);
  pid->setMaxIntegratedError(0.2 * max_thrust / pid->ki());
  pid->setMinIntegratedError(-0.2 * max_thrust / pid->ki());

  d->normal_depth_pid_gains_ = pidDown()->pidGains();
  setRovGainFactor(ros::param::param<double>("~rov_gain_factor", 1.3));

  param_name = ros::names::resolve(dynamics_ns, std::string{ "SENTRY_Cdu" });
  const auto vehicle_drag = ros::param::param<double>(param_name, 210);
  setVehicleDragCoefficient(vehicle_drag);

  //
  //  Set up our wing dynamics
  //

  auto wing = wingDynamics();

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Area" });
  const auto area = ros::param::param<double>(param_name, 0.2);
  wing->setWingArea(area);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Rho" });
  const auto density = ros::param::param<double>(param_name, 1000.0);
  wing->setFluidDensity(density);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cl0" });
  const auto cl0 = ros::param::param<double>(param_name, 0.95);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cl1" });
  const auto cl_low = ros::param::param<double>(param_name, 2.10);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cl2" });
  const auto cl_high = ros::param::param<double>(param_name, -2.48);

  wing->setLiftCoefficients(cl0, cl_low, cl_high);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_alpha1" });
  const auto cl_low_angle = ros::param::param<double>(param_name, 0.262);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_alpha2" });
  const auto cl_high_angle = ros::param::param<double>(param_name, 2.880);

  wing->setLiftAngleRegions(cl_low_angle, cl_high_angle);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cd0" });
  const auto cd0 = ros::param::param<double>(param_name, 1.8);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cd1" });
  const auto cd1 = ros::param::param<double>(param_name, 0.53);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cd2" });
  const auto cd2 = ros::param::param<double>(param_name, 0.25);

  wing->setDragCoefficients(cd0, cd1, cd2);

  setWingDynamics(std::move(wing));
}

void SentrySurveyController::setRovGainFactor(double gain)
{
  DS_D(SentrySurveyController);
  gain = std::abs(gain);
  d->rov_gain_factor_ = gain;

  d->rov_depth_pid_gains_.kp = gain * d->normal_depth_pid_gains_.kp;
  d->rov_depth_pid_gains_.ki = 0;
  d->rov_depth_pid_gains_.kd = std::sqrt(gain) * d->normal_depth_pid_gains_.kd;
}
double SentrySurveyController::rovGainFactor() const noexcept
{
  const DS_D(SentrySurveyController);
  return d->rov_gain_factor_;
}

void SentrySurveyController::setWingDynamics(std::unique_ptr<ds_util::WingDynamics> wing_dynamics)
{
  DS_D(SentrySurveyController);
  d->wing_dynamics_.swap(wing_dynamics);
}

std::unique_ptr<ds_util::WingDynamics> SentrySurveyController::wingDynamics() noexcept
{
  DS_D(SentrySurveyController);
  return std::unique_ptr<ds_util::WingDynamics>(new ds_util::WingDynamics(*d->wing_dynamics_));
}

double SentrySurveyController::vehicleDragCoefficient() const noexcept
{
  const DS_D(SentrySurveyController);
  return d->vehicle_drag_coefficient_;
}

void SentrySurveyController::setVehicleDragCoefficient(double value)
{
  DS_D(SentrySurveyController);
  d->vehicle_drag_coefficient_ = value;
}

double SentrySurveyController::estimateForwardVehicleDrag(double fwd_angle_of_attack, double aft_angle_of_attack,
                                                          double speed)
{
  const auto fwd_fins = estimateControlSurfaceDrag(fwd_angle_of_attack, speed);
  const auto aft_fins = estimateControlSurfaceDrag(aft_angle_of_attack, speed);
  const auto vehicle_drag = vehicleDragCoefficient();

  return 0.1 * vehicle_drag * speed + vehicle_drag * speed * std::abs(speed) + fwd_fins + aft_fins;
}

double SentrySurveyController::estimateControlSurfaceDrag(double angle_of_attack, double speed)
{
  DS_D(SentrySurveyController);
  return d->wing_dynamics_->calculate_drag(angle_of_attack, speed);
}

#if 0
double SentrySurveyController::estimateControlSurfaceLift(double angle_of_attack, double speed)
{
  DS_D(SentrySurveyController);
  return d->wing_dynamics_->calculate_lift(angle_of_attack, speed);
}
#endif

geometry_msgs::WrenchStamped SentrySurveyController::calculateForces(ds_nav_msgs::AggregatedState state,
                                                                     ds_nav_msgs::AggregatedState reference,
                                                                     ros::Duration dt)
{
  auto wrench = SurveyController::calculateForces(std::move(state), reference, std::move(dt));

  // Add the feed-forward term for the desired forward speed accounting for
  // actuator surface drag.

  // Add estimated vehicle drag as a feed-forward term.  Note!  The drag
  // calculation does *not* invert the sign of the force.
  //
  // The original ROV code used hard-coded angles depending on allocation.  We may want
  // to revisit this later and substitute actual servo angles.
  auto fwd_servo_angle = 0.0;
  auto aft_servo_angle = 0.0;
  switch (activeAllocation())
  {
    case SentryAllocation::AllocationType::FLIGHT_MODE:
      fwd_servo_angle = 0.0;
      aft_servo_angle = 0.0;
      break;
    case SentryAllocation::AllocationType::ROV_MODE:
      fwd_servo_angle = M_PI_2;
      aft_servo_angle = 0;
      break;
    case SentryAllocation::AllocationType::VERTICAL_MODE:
      fwd_servo_angle = M_PI_2;
      aft_servo_angle = M_PI_2;
      break;
    default:
      fwd_servo_angle = 0.0;
      aft_servo_angle = 0.0;
      break;
  }

  const auto estimated_drag = estimateForwardVehicleDrag(fwd_servo_angle, aft_servo_angle, reference.surge_u.value);

  wrench.wrench.force.x += estimated_drag;

  return wrench;
}

bool SentrySurveyController::switchAllocation(size_t id)
{
  // Don't do anything if switching allocations failed.
  if (!SurveyController::switchAllocation(id))
  {
    return false;
  }

  DS_D(SentrySurveyController);

  if (id == sentry_msgs::SentryAllocationEnum::ROV_MODE)
  {
    // Swap in ROV depth gains if that's the new active allocation
    ROS_WARN("Switching to depth PID gains for ROV mode.  kp: %.2f ki: %.2f kd: %.2f", d->rov_depth_pid_gains_.kp,
             d->rov_depth_pid_gains_.ki, d->rov_depth_pid_gains_.kd);
    pidDown()->setPidGains(d->rov_depth_pid_gains_);
  }
  else
  {
    // Otherwise use our depth normal gains.
    ROS_WARN("Switching to depth PID gains for normal (not ROV) mode.  kp: %.2f ki: %.2f kd: %.2f",
             d->normal_depth_pid_gains_.kp, d->normal_depth_pid_gains_.ki, d->normal_depth_pid_gains_.kd);
    pidDown()->setPidGains(d->normal_depth_pid_gains_);
  }

  return true;
}

void SentrySurveyController::bottomFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg)
{
  if (activeDepthGoal() == ds_control_msgs::DepthGoalEnum::CONSTANT_ALTITUDE)
  {
    SurveyController::bottomFollowerGoalCallback(msg);
  }
}

void SentrySurveyController::bottomFollowerStateCallback(const ds_control_msgs::BottomFollow1D& msg)
{
  // constant-altitude is not enabled, don't do anything
  if (activeDepthGoal() != ds_control_msgs::DepthGoalEnum::CONSTANT_ALTITUDE)
  {
    return;
  }

  SurveyController::bottomFollowerStateCallback(msg);

  // Allocation choice -- only when constant altitude goal is the active goal
  if (!enabled())
  {
    return;
  }

  DS_D(SentrySurveyController);
  d->initialize_allocation_for_constant_depth_goal_ = true;

  if (!msg.header.stamp.isValid())
  {
    ROS_WARN_STREAM("Ignoring bottom follower state message; invalid timestamp");
    return;
  }

  if (msg.alarm && (activeAllocation() != SentryAllocation::AllocationType::ROV_MODE))
  {
    ROS_WARN_STREAM("Bottom follower alarm set, switching to ROV allocation");
    if (!switchAllocation(SentryAllocation::AllocationType::ROV_MODE))
    {
      ROS_ERROR_STREAM("Failed to switch to ROV allocation!");
    }
    return;
  }

  if (!msg.alarm && (activeAllocation() != sentry_msgs::SentryAllocationEnum::FLIGHT_MODE))
  {
    ROS_WARN_STREAM("Bottom follower alarm cleared, switching to FLT1 allocation");
    if (!switchAllocation(SentryAllocation::AllocationType::FLIGHT_MODE))
    {
      ROS_ERROR_STREAM("Failed to switch to FLT1 allocation!");
      return;
    }
  }
}

void SentrySurveyController::depthFollowerGoalCallback(const ds_nav_msgs::AggregatedState& msg)
{
  if (activeDepthGoal() != ds_control_msgs::DepthGoalEnum::CONSTANT_DEPTH)
  {
    return;
  }
  SurveyController::bottomFollowerGoalCallback(msg);
}

void SentrySurveyController::depthFollowerStateCallback(const ds_control_msgs::DepthFollow& msg)
{
  // constant-altitude is not enabled, don't do anything
  if (activeDepthGoal() != ds_control_msgs::DepthGoalEnum::CONSTANT_DEPTH)
  {
    return;
  }

  //SurveyController::bottomFollowerStateCallback(msg);
  if (!msg.header.stamp.isValid())
  {
    return;
  }

  auto smoother = depthReferenceSmoother();

  const auto vel = msg.depth_rate == 0.0 ? defaultDepthSmootherVelocity() : msg.depth_rate;
  const auto acc = msg.depth_acceleration == 0.0 ? defaultDepthSmootherAcceleration() : msg.depth_acceleration;

  if (smoother->maxVelocity() != vel)
  {
    ROS_INFO("Setting velocity smoother max velocity to %.3f", vel);
    smoother->setMaxVelocity(vel);
  }

  if (smoother->maxAcceleration() != acc)
  {
    ROS_INFO("Setting velocity smoother max acceleration to %.3f", acc);
    smoother->setMaxAcceleration(acc);
  }

  // Allocation choice -- only when constant depth goal is the active goal
  if (!enabled())
  {
    return;
  }

  DS_D(SentrySurveyController);

  // already initialized
  if (!d->initialize_allocation_for_constant_depth_goal_)
  {
    return;
  }

  // Check if flight allocation is enabled.  if not, enable it.
  // Only do this once though.  The abort process switches to joystick controller with vertical allocation,
  // but the order of parameter changes the node gets isn't guaranteed.  If the allocation
  // switches before the controller and we're strictly allowing only flight allocation here then
  // we might not switch to vertical as intended during abort.
  //
  // This happened during the abort of sentry688
  if(activeAllocation() != sentry_msgs::SentryAllocationEnum::FLIGHT_MODE)
  {
    ROS_WARN_STREAM("Depth follower goal active, but FLT allocation not enabled, switching allocations.");
    if (!switchAllocation(SentryAllocation::AllocationType::FLIGHT_MODE))
    {
      ROS_ERROR_STREAM("Failed to switch to FLT1 allocation!");
      return;
    }
    ROS_INFO("flight allocation is enabled");
  }
  d->initialize_allocation_for_constant_depth_goal_ = false;
}

void SentrySurveyController::postEnableHook()
{
  // Every time we enable the survey controller, reset the reinitialize variable true
  DS_D(SentrySurveyController);
  ROS_INFO("SentrySurveyController postEnableHook: setting initialize_allocation_for_constant_depth_goal true");
  ROS_INFO("                                       will enable flight allocation if constant depth goal is enabled.");
  d->initialize_allocation_for_constant_depth_goal_ = true;
  SurveyController::postEnableHook();
}

void SentrySurveyController::setupSubscriptions()
{
  SurveyController::setupSubscriptions();

  DS_D(SentrySurveyController);
  if (activeAllocation() == SentryAllocation::AllocationType::ROV_MODE)
  {
    pidDown()->setPidGains(d->rov_depth_pid_gains_);
  }
}

void SentrySurveyController::setupReferences()
{
  SurveyController::setupReferences();

  const auto& refs = referenceMap();

  auto nh = nodeHandle();
  DS_D(SentrySurveyController);

  auto it = refs.find("depth_follower");
  if (it == std::end(refs))
  {
    ROS_FATAL("No 'depth_follower' reference provided for survey controller");
    ROS_BREAK();
  }
  auto topic = ros::names::resolve(it->second, std::string{ "goal_state" });
  d->depth_follower_ref_sub_ = nh.subscribe(topic, 1, &SentrySurveyController::depthFollowerGoalCallback, this);

  topic = ros::names::resolve(it->second, std::string{ "state" });
  d->depth_follower_state_sub_ = nh.subscribe(topic, 1, &SentrySurveyController::depthFollowerStateCallback, this);
  
}

size_t SentrySurveyController::activeDepthGoal() const noexcept
{
  const DS_D(SentrySurveyController);
  return static_cast<size_t>(d->active_depth_goal_);
}

void SentrySurveyController::handleParameterUpdate(const ds_param::ParamConnection::ParamCollection& params)
{
    if (params.empty())
    {
      return;
    }
    DS_D(SentrySurveyController);
    d->active_depth_goal_ = d->param_active_depth_goal_->Get();
}
}
