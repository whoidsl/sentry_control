/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_joystick_controller.h"
#include "sentry_joystick_controller_private.h"

namespace sentry_control
{
SentryJoystickController::SentryJoystickController()
  : JoystickController(), d_ptr_(std::unique_ptr<SentryJoystickControllerPrivate>(new SentryJoystickControllerPrivate))
{
}
SentryJoystickController::SentryJoystickController(int argc, char** argv, const std::string& name)
  : JoystickController(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryJoystickControllerPrivate>(new SentryJoystickControllerPrivate))
{
}

void SentryJoystickController::setup()
{
  JoystickController::setup();
  setWrenchFrameId("base_link_NED");
}

void SentryJoystickController::setupParameters()
{
  JoystickController::setupParameters();

  const auto dynamics_ns = ros::param::param<std::string>("dynamics_ns", "");
  auto param_name = ros::names::resolve(dynamics_ns, std::string{ "max_thruster_force" });
  const auto max_thrust = std::abs(ros::param::param<double>(param_name, 80));

  param_name = ros::names::resolve(dynamics_ns, std::string{ "max_speed" });
  auto max_speed = std::abs(ros::param::param<double>(param_name, 1.5));
  if (max_speed == 0.0)
  {
    ROS_FATAL_STREAM(param_name << " is 0");
    ROS_BREAK();
  }
  DS_D(SentryJoystickController);
  d->max_speed_ = max_speed;

  auto force_scale = geometry_msgs::Wrench{};
  force_scale.force.x = 4 * max_thrust;
  force_scale.force.z = 6 * max_thrust;
  force_scale.torque.z = 2 * max_thrust;
  setForceScale(force_scale);

  auto pid = pidHeading();
  pid->setMaxSaturation(2 * max_thrust);
  pid->setMinSaturation(-2 * max_thrust);
}

void SentryJoystickController::updateReference(const ds_nav_msgs::AggregatedState& msg)
{
  JoystickController::updateReference(msg);
}

SentryJoystickController::~SentryJoystickController() = default;
}
