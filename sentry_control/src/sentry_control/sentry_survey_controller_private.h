/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_SURVEY_CONTROLLER_PRIVATE_H
#define SENTRY_CONTROL_SENTRY_SURVEY_CONTROLLER_PRIVATE_H

#include "ds_util/dynamics.h"
#include "ds_param/ds_param_conn.h"
#include "ds_control_msgs/DepthGoalEnum.h"

namespace sentry_control
{
struct SentrySurveyControllerPrivate
{
  SentrySurveyControllerPrivate()
    : rov_gain_factor_(1.0)
    , wing_dynamics_(std::unique_ptr<ds_util::WingDynamics>(new ds_util::WingDynamics(0.2, 1000.0)))
    , vehicle_drag_coefficient_(100.0)
  {
  }
  ~SentrySurveyControllerPrivate() = default;

  double rov_gain_factor_ = 1.0;
  ds_control::Pid::PidGains rov_depth_pid_gains_;
  ds_control::Pid::PidGains normal_depth_pid_gains_;

  std::unique_ptr<ds_util::WingDynamics> wing_dynamics_;
  double vehicle_drag_coefficient_;

  ds_param::ParamConnection::Ptr param_sub_;
  ds_param::IntParam::Ptr param_active_depth_goal_;
  int active_depth_goal_;
  bool initialize_allocation_for_constant_depth_goal_ = true;
  ros::Subscriber depth_follower_ref_sub_;
  ros::Subscriber depth_follower_state_sub_;
};
}

#endif  // SENTRY_CONTROL_SENTRY_SURVEY_CONTROLLER_PRIVATE_H
