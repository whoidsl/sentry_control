/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_CONTROL_SENTRY_ALLOCATION_PRIVATE_H
#define SENTRY_CONTROL_SENTRY_ALLOCATION_PRIVATE_H

#include "sentry_control/sentry_allocation.h"
#include "ds_util/dynamics.h"
#include "ds_param/ds_param_conn.h"

namespace sentry_control
{
struct SentryAllocationPrivate
{
  explicit SentryAllocationPrivate()
    : alpha_(-16.25)
    , beta_(8.93)
    , max_joybox_speed_(1.0)
    , max_thruster_force_(80.0)
    , ttl_seconds_(60)
    , max_servo_angle_(M_PI_2)
    , moment_arms_(DEFAULT_SENTRY_MOMENT_ARMS)
    , wing_lift_coefficient_(210.0)
  {
    pos_force_is_pos_current_.fill(true);
  }
  ~SentryAllocationPrivate() = default;

  double max_thruster_force_;
  double max_thruster_current_;
  double max_servo_angle_;

  double alpha_;
  double beta_;

  double fwd_speed_ref_;
  double max_joybox_speed_;
  double wing_lift_coefficient_;

  double ttl_seconds_;

  SentryAllocation::ThrusterParam moment_arms_;
  SentryAllocation::ServoParam servo_gains_;
  SentryAllocation::ThrusterParam heading_gains_;

  std::array<geometry_msgs::Wrench, SentryAllocation::NUM_THRUSTERS> thruster_wrench_gains_;
  std::array<geometry_msgs::Wrench, SentryAllocation::NUM_SERVOS> servo_wrench_gains_;

  std::array<ros::Publisher, SentryAllocation::NUM_THRUSTERS> thruster_pub_;
  std::array<ros::Publisher, SentryAllocation::NUM_SERVOS> servo_pub_;
  std::array<bool, SentryAllocation::NUM_THRUSTERS> pos_force_is_pos_current_;

  ros::Subscriber reference_sub_;

  ds_param::ParamConnection::Ptr param_sub_;
  ds_param::IntParam::Ptr active_controller_;
};
}

#endif  // SENTRY_CONTROL_SENTRY_ALLOCATION_PRIVATE_H
