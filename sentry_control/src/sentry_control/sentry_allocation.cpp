/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_allocation.h"
#include "sentry_msgs/SentryControllerEnum.h"
#include "sentry_allocation_private.h"

#include "ds_util/thruster.h"

#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_actuator_msgs/ServoCmd.h"

#include <ros/ros.h>
#include <urdf/model.h>

#include <algorithm>

namespace sentry_control
{
SentryAllocation::SentryAllocation()
  : AllocationBase(), d_ptr_(std::unique_ptr<SentryAllocationPrivate>(new SentryAllocationPrivate))
{
  setMaxThrusterForce(80.0);
  setMaxThrusterCurrent(10.0);
}

SentryAllocation::SentryAllocation(const Gains& gains)
  : AllocationBase(), d_ptr_(std::unique_ptr<SentryAllocationPrivate>(new SentryAllocationPrivate))
{
  setGains(gains);
}

SentryAllocation::SentryAllocation(int argc, char* argv[], const std::string& name)
  : AllocationBase(argc, argv, name), d_ptr_(std::unique_ptr<SentryAllocationPrivate>(new SentryAllocationPrivate))
{
  setMaxThrusterForce(80.0);
  setMaxThrusterCurrent(10.0);
}

SentryAllocation::~SentryAllocation() = default;

void SentryAllocation::setMomentArms(SentryAllocation::ThrusterParam arms)
{
  DS_D(SentryAllocation);
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    const auto thruster = to_string(static_cast<ThrusterParamIndex>(i));
    ROS_INFO_STREAM("Setting " << thruster << " moment arm to " << arms[i]);
    d->thruster_wrench_gains_[i].torque.z = d->heading_gains_[i] / arms[i];
  }
  d->moment_arms_ = arms;
}

SentryAllocation::ThrusterParam SentryAllocation::momentArms() const noexcept
{
  const DS_D(SentryAllocation);
  return d->moment_arms_;
}

void SentryAllocation::setCurrentCoefficients(double alpha, double beta)
{
  DS_D(SentryAllocation);
  d->alpha_ = alpha;
  d->beta_ = beta;
}

std::pair<double, double> SentryAllocation::currentCoefficients() const noexcept
{
  const DS_D(SentryAllocation);
  return { d->alpha_, d->beta_ };
}

void SentryAllocation::setMaxSpeed(double speed)
{
  DS_D(SentryAllocation);
  d->max_joybox_speed_ = std::fabs(speed);
}

double SentryAllocation::maxSpeed() const noexcept
{
  const DS_D(SentryAllocation);
  return d->max_joybox_speed_;
}

void SentryAllocation::setMaxThrusterCurrent(double amps)
{
  DS_D(SentryAllocation);
  amps = std::abs(amps);
  ROS_DEBUG_STREAM("Setting max thruster current to " << amps);
  d->max_thruster_current_ = amps;
}

double SentryAllocation::maxThrusterCurrent() const noexcept
{
  const DS_D(SentryAllocation);
  return d->max_thruster_current_;
}

void SentryAllocation::setThrusterTTL(double seconds)
{
  DS_D(SentryAllocation);
  d->ttl_seconds_ = seconds;
}

double SentryAllocation::thrusterTTL() const noexcept
{
  const DS_D(SentryAllocation);
  return d->ttl_seconds_;
}

void SentryAllocation::setWingLiftCoefficient(double newtons)
{
  DS_D(SentryAllocation);
  d->wing_lift_coefficient_ = std::abs(newtons);
}

double SentryAllocation::wingLiftCoefficient() const noexcept
{
  const DS_D(SentryAllocation);
  return d->wing_lift_coefficient_;
}

double SentryAllocation::currentFromForce(double force, double speed) const noexcept
{
  const DS_D(SentryAllocation);

  // Make sure we're non-negative.
  speed = std::fabs(speed);
  if (speed > d->max_joybox_speed_)
  {
    ROS_WARN("Clipping speed of %.2f to max allowed: %.2f", speed, d->max_joybox_speed_);
    speed = d->max_joybox_speed_;
  }

  const auto max_force = force > 0 ? maxThrusterForce() : -maxThrusterForce();
  if (std::abs(force) > std::abs(max_force))
  {
    ROS_WARN("Clipping force of %.2f to max allowed: %.2f", force, max_force);
    force = max_force;
  }

  auto current = ds_util::linear_force_to_current(d->alpha_, d->beta_, force, speed);

  const auto max_current = current > 0 ? maxThrusterCurrent() : -maxThrusterCurrent();
  if (std::abs(current) > std::abs(max_current))
  {
    ROS_WARN("Clipping current of %.2f to max allowed: %.2f", current, max_current);
    current = max_current;
  }

  return current;
}

double SentryAllocation::forceFromCurrent(double current, double speed) const noexcept
{
  const DS_D(SentryAllocation);

  // Make sure we're non-negative.
  speed = std::fabs(speed);
  if (speed > d->max_joybox_speed_)
  {
    ROS_WARN("Clipping speed of %.2f to max allowed: %.2f", speed, d->max_joybox_speed_);
    speed = d->max_joybox_speed_;
  }

  const auto max_current = current > 0 ? maxThrusterCurrent() : -maxThrusterCurrent();
  if (std::abs(current) > std::abs(max_current))
  {
    ROS_WARN("Clipping current of %.2f to max allowed: %.2f", current, max_current);
    current = max_current;
  }

  auto force = ds_util::linear_current_to_force(d->alpha_, d->beta_, current, speed);

  const auto max_force = force > 0 ? maxThrusterForce() : -maxThrusterForce();
  if (std::abs(force) > std::abs(max_force))
  {
    ROS_WARN("Clipping force of %.2f to max allowed: %.2f", force, max_force);
    force = max_force;
  }

  return force;
}

void SentryAllocation::balanceSteeringThrusters(double& port, double& stbd)
{
  const auto max_allowed_thrust = maxThrusterForce();

  // This was the way balancing was done in the old ROV code ('fix_saturation' in feadback.cpp).
  const auto max_thrust = std::max(port, stbd);

  if (max_thrust > max_allowed_thrust)
  {
    port = port - (max_thrust - max_allowed_thrust);
    stbd = stbd - (max_thrust - max_allowed_thrust);
  }

  const auto min_thrust = std::min(port, stbd);
  if (min_thrust < -max_allowed_thrust)
  {
    port = port - (min_thrust + max_allowed_thrust);
    stbd = stbd - (min_thrust + max_allowed_thrust);
  }
}

void SentryAllocation::setForwardGain(ThrusterParam gains)
{
  DS_D(SentryAllocation);

  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    d->thruster_wrench_gains_[i].force.x = gains[i];
  }
}

SentryAllocation::ThrusterParam SentryAllocation::forwardGain() const noexcept
{
  auto gains = ThrusterParam{};
  const DS_D(SentryAllocation);
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    gains[i] = d->thruster_wrench_gains_[i].force.x;
  }

  return gains;
}

void SentryAllocation::setVerticalGain(ThrusterParam gains)
{
  DS_D(SentryAllocation);
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    d->thruster_wrench_gains_[i].force.z = gains[i];
  }
}

SentryAllocation::ThrusterParam SentryAllocation::verticalGain() const noexcept
{
  auto gains = ThrusterParam{};
  const DS_D(SentryAllocation);

  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    gains[i] = d->thruster_wrench_gains_[i].force.z;
  }

  return gains;
}

void SentryAllocation::setHeadingGain(ThrusterParam gains)
{
  auto moment_arms = momentArms();

  DS_D(SentryAllocation);

  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    d->heading_gains_[i] = gains[i];
    d->thruster_wrench_gains_[i].torque.z = d->heading_gains_[i] / moment_arms[i];
  }
}

SentryAllocation::ThrusterParam SentryAllocation::headingGain() const noexcept
{
  const DS_D(SentryAllocation);
  return d->heading_gains_;
}

void SentryAllocation::setServoGain(SentryAllocation::ServoParam gains)
{
  DS_D(SentryAllocation);
  for (auto i = 0; i < NUM_SERVOS; ++i)
  {
    d->servo_gains_[i] = gains[i];
    d->servo_wrench_gains_[i].force.z = d->servo_gains_[i] / d->wing_lift_coefficient_;
  }
}

SentryAllocation::ServoParam SentryAllocation::servoGain() const noexcept
{
  const DS_D(SentryAllocation);
  return d->servo_gains_;
}

void SentryAllocation::setGains(Gains gains)
{
  setForwardGain(gains.thruster_force_u);
  setVerticalGain(gains.thruster_force_w);
  setHeadingGain(gains.thruster_torque_w);
  setServoGain(gains.servo_force_w);
}

SentryAllocation::Gains SentryAllocation::gains() const noexcept
{
  auto gains = Gains{};
  gains.thruster_force_u = forwardGain();
  gains.thruster_force_w = verticalGain();
  gains.thruster_torque_w = headingGain();
  gains.servo_force_w = servoGain();

  return gains;
}

const std::array<geometry_msgs::Wrench, SentryAllocation::NUM_THRUSTERS>& SentryAllocation::thrusterWrenchGains() const
    noexcept
{
  const DS_D(SentryAllocation);
  return d->thruster_wrench_gains_;
}
const std::array<geometry_msgs::Wrench, SentryAllocation::NUM_SERVOS>& SentryAllocation::servoWrenchGains() const
    noexcept
{
  const DS_D(SentryAllocation);
  return d->servo_wrench_gains_;
}

SentryAllocation::ThrusterParam SentryAllocation::calculateThrusterForces(const geometry_msgs::Wrench& wrench)
{
  // auto wrench = applyForceInputLimits(wrench);

  auto output = SentryAllocation::ThrusterParam{};
  const auto& gains = thrusterWrenchGains();

  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    const auto& gain = gains.at(i);
    output[i] = wrench.force.x * gain.force.x + wrench.force.y * gain.force.y + wrench.force.z * gain.force.z +
                wrench.torque.x * gain.torque.x + wrench.torque.y * gain.torque.y + wrench.torque.z * gain.torque.z;
  }

  //ROS_ERROR_STREAM("Pre-balance: " << output.at(PARAM_THRUSTER_PORT_AFT) << " " << output.at(PARAM_THRUSTER_PORT_AFT));
  balanceSteeringThrusters(output.at(PARAM_THRUSTER_PORT_AFT), output.at(PARAM_THRUSTER_STBD_AFT));
  //ROS_ERROR_STREAM("Post-balance: " << output.at(PARAM_THRUSTER_PORT_AFT) << " " << output.at(PARAM_THRUSTER_PORT_AFT));
  balanceSteeringThrusters(output.at(PARAM_THRUSTER_PORT_FWD), output.at(PARAM_THRUSTER_STBD_FWD));

  return output;
}

SentryAllocation::ServoParam SentryAllocation::calculateServoAngles(const geometry_msgs::Wrench& wrench)
{
  // auto wrench = applyForceInputLimits(wrench_);
  auto output = SentryAllocation::ServoParam{};
  const auto& gains = servoWrenchGains();

  const auto angle_limit = maxServoAngle();
  for (auto i = 0; i < NUM_SERVOS; ++i)
  {
    const auto& gain = gains.at(i);
    output[i] = wrench.force.x * gain.force.x + wrench.force.y * gain.force.y + wrench.force.z * gain.force.z +
                wrench.torque.x * gain.torque.x + wrench.torque.y * gain.torque.y + wrench.torque.z * gain.torque.z;
    output[i] = std::min(output[i], angle_limit);
    output[i] = std::max(output[i], -angle_limit);
  }

  return output;
}

void SentryAllocation::publishThrusterCommands(const std::array<ds_actuator_msgs::ThrusterCmd, NUM_THRUSTERS>& commands)
{
  DS_D(SentryAllocation);
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    d->thruster_pub_.at(i).publish(commands.at(i));
  }
}

void SentryAllocation::publishServoCommands(const std::array<ds_actuator_msgs::ServoCmd, NUM_SERVOS>& commands)
{
  DS_D(SentryAllocation);
  for (auto i = 0; i < NUM_SERVOS; ++i)
  {
    d->servo_pub_.at(i).publish(commands.at(i));
  }
}

void SentryAllocation::publishActuatorCommands(geometry_msgs::WrenchStamped body_forces)
{
  if (!enabled())
  {
    return;
  }

  const auto clipped_forces = applyForceInputLimits(body_forces.wrench);
  const auto thruster_forces = calculateThrusterForces(clipped_forces);

  DS_D(SentryAllocation);
  const auto speed = d->fwd_speed_ref_;
  auto thruster_currents = ThrusterParam{};
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    const auto& force = thruster_forces.at(i);
    thruster_currents[i] = currentFromForce(force, speed);
    if (!d->pos_force_is_pos_current_.at(i))
    {
      thruster_currents[i] *= -1;
    }
  }

  const auto now = ros::Time::now();
  auto thruster_cmd = ds_actuator_msgs::ThrusterCmd{};
  thruster_cmd.stamp = now;
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    thruster_cmd.cmd_newtons = static_cast<float>(thruster_forces.at(i));
    thruster_cmd.cmd_value = static_cast<float>(thruster_currents.at(i));
    thruster_cmd.thruster_name = to_string(static_cast<ThrusterParamIndex>(i));
    thruster_cmd.ttl_seconds = d->ttl_seconds_;
    d->thruster_pub_.at(i).publish(thruster_cmd);
  }

  const auto servo_angles = calculateServoAngles(body_forces.wrench);
  auto servo_cmd = ds_actuator_msgs::ServoCmd{};
  servo_cmd.stamp = now;
  for (auto i = 0; i < NUM_SERVOS; ++i)
  {
    servo_cmd.servo_name = to_string(static_cast<ServoParamIndex>(i));
    servo_cmd.cmd_radians = static_cast<float>(servo_angles.at(i));
    d->servo_pub_.at(i).publish(servo_cmd);
  }
}

void SentryAllocation::setReferenceSpeed(double speed)
{
  DS_D(SentryAllocation);
  speed = std::min(d->max_joybox_speed_, std::abs(speed));
  d->fwd_speed_ref_ = speed;
}

double SentryAllocation::referenceSpeed() const noexcept
{
  const DS_D(SentryAllocation);
  return d->fwd_speed_ref_;
}

void SentryAllocation::setMaxThrusterForce(double newtons)
{
  DS_D(SentryAllocation);
  newtons = std::abs(newtons);
  ROS_DEBUG_STREAM("Setting max thruster force to " << newtons);
  d->max_thruster_force_ = newtons;
}

double SentryAllocation::maxThrusterForce() const noexcept
{
  const DS_D(SentryAllocation);
  return d->max_thruster_force_;
}

void SentryAllocation::setMaxServoAngle(double radians)
{
  DS_D(SentryAllocation);
  radians = std::abs(radians);
  ROS_DEBUG_STREAM("Setting max servo angle to " << radians);
  d->max_servo_angle_ = radians;
}

double SentryAllocation::maxServoAngle() const noexcept
{
  const DS_D(SentryAllocation);
  return d->max_servo_angle_;
}

void SentryAllocation::setPosForceIsPosCurrent(std::array<bool, NUM_THRUSTERS> currents)
{
  DS_D(SentryAllocation);
  d->pos_force_is_pos_current_ = std::move(currents);
}

std::array<bool, SentryAllocation::NUM_THRUSTERS> SentryAllocation::posForceIsPosCurrent() const noexcept
{
  const DS_D(SentryAllocation);
  return d->pos_force_is_pos_current_;
}

void SentryAllocation::referenceUpdate(const ds_nav_msgs::AggregatedState& ref_msg)
{
  DS_D(SentryAllocation);
  if (d->active_controller_->Get() == sentry_msgs::SentryControllerEnum::JOYSTICK)
  {
    setReferenceSpeed(d->max_joybox_speed_);
    return;
  }

  if (ref_msg.surge_u.valid)
  {
    setReferenceSpeed(ref_msg.surge_u.value);
  }
  else
  {
    setReferenceSpeed(d->max_joybox_speed_);
  }
}

namespace detail
{
std::pair<double, bool> get_moment_arm(urdf::Model& model, const std::string& port_stbd, const std::string& fwd_aft)
{
  auto joint_name = fwd_aft + "_wing_to_" + fwd_aft + "_" + port_stbd + "_fin";
  auto joint_ptr = model.getJoint(joint_name);
  if (!joint_ptr)
  {
    ROS_ERROR_STREAM("Unable to get joint '" << joint_name << "'");
    return { 0, false };
  }

  auto arm = joint_ptr->parent_to_joint_origin_transform.position.y;
  joint_name = fwd_aft + "_" + port_stbd + "_fin_to_thruster";

  joint_ptr = model.getJoint(joint_name);
  if (!joint_ptr)
  {
    ROS_ERROR_STREAM("Unable to get joint '" << joint_name << "'");
    return { 0, false };
  }

  arm += joint_ptr->parent_to_joint_origin_transform.position.y;
  ROS_DEBUG_STREAM("thruster_" << fwd_aft << "_" << port_stbd << " moment arm is: " << arm);
  return { arm, true };
};
}

bool SentryAllocation::readRobotDescription(const std::string& param)
{
  if (!ros::param::has(param))
  {
    ROS_ERROR_STREAM("Unable to load dynamics from URDF model.  No parameter named '" << param << "'");
    return false;
  }

  auto model = urdf::Model{};
  if (!model.initParam(param))
  {
    ROS_ERROR_STREAM("Invalid URDF model loaded into parameter '" << param << "'");
    return false;
  }

  auto port_fwd_arm = 0.0;
  auto port_aft_arm = 0.0;
  auto stbd_fwd_arm = 0.0;
  auto stbd_aft_arm = 0.0;

  auto ok = false;

  std::tie(port_fwd_arm, ok) = detail::get_moment_arm(model, "port", "fwd");

  if (!ok)
  {
    return false;
  }
  std::tie(port_aft_arm, ok) = detail::get_moment_arm(model, "port", "aft");

  if (!ok)
  {
    return false;
  }
  std::tie(stbd_aft_arm, ok) = detail::get_moment_arm(model, "stbd", "aft");

  if (!ok)
  {
    return false;
  }
  std::tie(stbd_fwd_arm, ok) = detail::get_moment_arm(model, "stbd", "fwd");

  if (!ok)
  {
    return false;
  }

  auto arms = ThrusterParam{};
  arms[ThrusterParamIndex::PARAM_THRUSTER_PORT_FWD] = port_fwd_arm;
  arms[ThrusterParamIndex::PARAM_THRUSTER_PORT_AFT] = port_aft_arm;
  arms[ThrusterParamIndex::PARAM_THRUSTER_STBD_FWD] = stbd_fwd_arm;
  arms[ThrusterParamIndex::PARAM_THRUSTER_STBD_AFT] = stbd_aft_arm;

  setMomentArms(arms);
  return true;
}

std::string to_string(SentryAllocation::ThrusterParamIndex index)
{
  switch (index)
  {
    case SentryAllocation::ThrusterParamIndex::PARAM_THRUSTER_STBD_FWD:
      return "thruster_fwd_stbd";
    case SentryAllocation::ThrusterParamIndex::PARAM_THRUSTER_STBD_AFT:
      return "thruster_aft_stbd";
    case SentryAllocation::ThrusterParamIndex::PARAM_THRUSTER_PORT_FWD:
      return "thruster_fwd_port";
    case SentryAllocation::ThrusterParamIndex::PARAM_THRUSTER_PORT_AFT:
      return "thruster_aft_port";
    default:
      return "";
  }
}

std::ostream& operator<<(std::ostream& os, const SentryAllocation::ThrusterParamIndex& index)
{
  os << to_string(index);
  return os;
}

std::string to_string(SentryAllocation::ServoParamIndex index)
{
  switch (index)
  {
    case SentryAllocation::ServoParamIndex::PARAM_SERVO_FWD:
      return "servo_fwd";
    case SentryAllocation::ServoParamIndex::PARAM_SERVO_AFT:
      return "servo_aft";
    default:
      return "";
  }
}

std::ostream& operator<<(std::ostream& os, const SentryAllocation::ServoParamIndex& index)
{
  os << to_string(index);
  return os;
}

#if 0
void SentryAllocation::setupFromParameterServer(ros::NodeHandle& nh)
{
  param_name = ros::names::resolve(dynamics_ns, std::string{ "SENTRY_Cdu" });
  double_param_value = vehicleDragCoefficient();
  if(ros::param::get(param_name, double_param_value))
  {
    setVehicleDragCoefficient(double_param_value);
  }

  //
  //  Set up our wing dynamics
  //

  auto wing = wingDynamics();


  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Area" });
  const auto area = ros::param::param<double>(param_name, 0.2);
  wing->setWingArea(area);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Rho" });
  const auto density = ros::param::param<double>(param_name, 1000.0);
  wing->setFluidDensity(density);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cl0" });
  const auto cl0 = ros::param::param<double>(param_name, 0.95);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cl1" });
  const auto cl_low = ros::param::param<double>(param_name, 2.10);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cl2" });
  const auto cl_high = ros::param::param<double>(param_name, -2.48);

  wing->setLiftCoefficients(cl0, cl_low, cl_high);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_alpha1" });
  const auto cl_low_angle = ros::param::param<double>(param_name, 0.262);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_alpha2" });
  const auto cl_high_angle = ros::param::param<double>(param_name, 2.880);

  wing->setLiftAngleRegions(cl_low_angle, cl_high_angle);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cd0" });
  const auto cd0 = ros::param::param<double>(param_name, 1.8);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cd1" });
  const auto cd1 = ros::param::param<double>(param_name, 0.53);

  param_name = ros::names::resolve(dynamics_ns, std::string{ "CS_Cd2" });
  const auto cd2 = ros::param::param<double>(param_name, 0.25);

  wing->setDragCoefficients(cd0, cd1, cd2);

  setWingDynamics(std::move(wing));
}
#endif
void SentryAllocation::setupParameters()
{
  DsProcess::setupParameters();

  const auto dynamics_ns = ros::param::param<std::string>("dynamics_ns", "");

  auto double_param_value = 0.0;
  double_param_value = maxThrusterForce();
  auto param_name = ros::names::resolve(dynamics_ns, std::string{ "max_thruster_force" });
  if (ros::param::get(param_name, double_param_value))
  {
    setMaxThrusterForce(double_param_value);
  }

  param_name = ros::names::resolve(dynamics_ns, std::string{ "max_thruster_current" });
  double_param_value = maxThrusterCurrent();
  if (ros::param::get(param_name, double_param_value))
  {
    setMaxThrusterCurrent(double_param_value);
  }
  param_name = ros::names::resolve(dynamics_ns, std::string{ "thruster_ttl" });
  double_param_value = thrusterTTL();
  if (ros::param::get(param_name, double_param_value))
  {
    setThrusterTTL(double_param_value);
  }

  auto alpha_beta = currentCoefficients();
  param_name = ros::names::resolve(dynamics_ns, std::string{ "force_to_amps_transform/alpha" });
  ros::param::get(param_name, alpha_beta.first);
  param_name = ros::names::resolve(dynamics_ns, std::string{ "force_to_amps_transform/beta" });
  ros::param::get(param_name, alpha_beta.second);
  setCurrentCoefficients(alpha_beta.first, alpha_beta.second);

  auto current_sign = std::array<bool, NUM_THRUSTERS>{};
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    const auto thruster_name = to_string(static_cast<ThrusterParamIndex>(i));
    auto value = false;
    param_name = ros::names::resolve(dynamics_ns, std::string{ "pos_force_is_pos_current/" + thruster_name });
    if (!ros::param::get(param_name, value))
    {
      continue;
    }

    current_sign[i] = value;
  }
  setPosForceIsPosCurrent(current_sign);

  param_name = "/robot_description";
  readRobotDescription(param_name);

  //
  // Allocation-specific parameters
  //
  param_name = std::string{ "~max_servo_angle_deg" };
  double_param_value = maxServoAngle();
  if (ros::param::get(param_name, double_param_value))
  {
    double_param_value *= M_PI / 180;
    setMaxServoAngle(double_param_value);
  }
}

void SentryAllocation::setupPublishers()
{
  DsProcess::setupPublishers();

  const auto actuator_ns = ros::param::param<std::string>("actuator_ns", "");
  DS_D(SentryAllocation);
  auto nh = nodeHandle();
  // setup thruster publishers
  for (auto i = 0; i < NUM_THRUSTERS; ++i)
  {
    const auto thruster_name = to_string(static_cast<ThrusterParamIndex>(i));
    const auto topic_name = ros::names::resolve(actuator_ns, thruster_name);
    d->thruster_pub_[i] = nh.advertise<ds_actuator_msgs::ThrusterCmd>(topic_name + "/cmd", 1, false);
  }

  // setup servo publishers
  for (auto i = 0; i < NUM_SERVOS; ++i)
  {
    const auto servo_name = to_string(static_cast<ServoParamIndex>(i));
    const auto topic_name = ros::names::resolve(actuator_ns, servo_name);
    d->servo_pub_[i] = nh.advertise<ds_actuator_msgs::ServoCmd>(topic_name + "/cmd", 1, false);
  }
}
void SentryAllocation::setupSubscriptions()
{
  AllocationBase::setupSubscriptions();
  const auto controller_ns = ros::param::param<std::string>("controller_ns", "");
  const auto topic_name = ros::names::resolve(controller_ns, std::string{ "reference" });
  DS_D(SentryAllocation);
  auto nh = nodeHandle();
  d->reference_sub_ = nh.subscribe(topic_name, 1, &SentryAllocation::referenceUpdate, this);

  d->param_sub_ = ds_param::ParamConnection::create(nh);
  d->active_controller_ = d->param_sub_->connect<ds_param::IntParam>("active_controller");
  d->param_sub_->setCallback(boost::bind(&SentryAllocation::parameterSubscriptionCallback, this, _1));
}

}  // namespace sentry_control
