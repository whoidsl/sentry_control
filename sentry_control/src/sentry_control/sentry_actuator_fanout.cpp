/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_actuator_fanout.h"
#include "sentry_control/sentry_thruster_allocation.h"

#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_actuator_msgs/ServoCmd.h"

#include "sentry_actuator_fanout_private.h"

namespace sentry_control
{
SentryActuatorFanout::SentryActuatorFanout()
  : DsProcess(), d_ptr_(std::unique_ptr<SentryActuatorFanoutPrivate>(new SentryActuatorFanoutPrivate))
{
}

SentryActuatorFanout::SentryActuatorFanout(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<SentryActuatorFanoutPrivate>(new SentryActuatorFanoutPrivate))
{
}

SentryActuatorFanout::~SentryActuatorFanout() = default;

void SentryActuatorFanout::setupSubscriptions()
{
  DsProcess::setupSubscriptions();

  const auto controller_ns = ros::param::param<std::string>("controller_ns", "");
  const auto topic_name = ros::names::resolve(controller_ns, std::string{ "actuator_inputs" });

  auto nh = nodeHandle();
  DS_D(SentryActuatorFanout);
  d->actuator_inputs_sub_ = nh.subscribe(topic_name, 1, &SentryActuatorFanout::sendActuatorCommands, this);
}

void SentryActuatorFanout::setupPublishers()
{
  DsProcess::setupPublishers();

  const auto actuator_ns = ros::param::param<std::string>("actuator_ns", "");
  DS_D(SentryActuatorFanout);

  auto nh = nodeHandle();
  for (auto i = 0; i < SentryThrusterAllocation::NUM_THRUSTERS; ++i)
  {
    const auto index = SentryThrusterAllocation::THRUSTER_FORCE_START_INDEX + i;
    const auto actuator_index = static_cast<SentryThrusterAllocation::ActuatorInputIndex>(index);
    const auto topic_name = ros::names::resolve(actuator_ns, sentry_control::to_string(actuator_index));

    d->thrusters_pub_[i] = nh.advertise<ds_actuator_msgs::ThrusterCmd>(topic_name, 1, false);
  }

  for (auto i = 0; i < SentryThrusterAllocation::NUM_SERVOS; ++i)
  {
    const auto index = SentryThrusterAllocation::SERVO_START_INDEX + i;
    const auto actuator_index = static_cast<SentryThrusterAllocation::ActuatorInputIndex>(index);
    const auto topic_name = ros::names::resolve(actuator_ns, sentry_control::to_string(actuator_index));

    d->servos_pub_[i] = nh.advertise<ds_actuator_msgs::ServoCmd>(topic_name, 1, false);
  }
}

void SentryActuatorFanout::sendActuatorCommands(const ds_control::ActuatorInputs& msg)
{
  if (msg.inputs.size() != SentryThrusterAllocation::NUM_ACTUATOR_INPUTS)
  {
    ROS_ERROR("Ignoring input message with improper size.  Expected %d, received %lu",
              SentryThrusterAllocation::NUM_ACTUATOR_INPUTS, msg.inputs.size());
  }
  auto thruster_msg = ds_actuator_msgs::ThrusterCmd{};
  thruster_msg.stamp = msg.header.stamp;
  DS_D(SentryActuatorFanout);

  for (auto i = 0; i < SentryThrusterAllocation::NUM_THRUSTERS; ++i)
  {
    const auto force_index = SentryThrusterAllocation::THRUSTER_FORCE_START_INDEX + i;
    const auto current_index = SentryThrusterAllocation::THRUSTER_CURRENT_START_INDEX + i;
    const auto actuator_index = static_cast<SentryThrusterAllocation::ActuatorInputIndex>(force_index);

    thruster_msg.thruster_name = sentry_control::to_string(actuator_index);

    thruster_msg.cmd_value = static_cast<float>(msg.inputs.at(current_index));
    thruster_msg.cmd_newtons = static_cast<float>(msg.inputs.at(force_index));
    d->thrusters_pub_.at(i).publish(thruster_msg);
  }

  auto servo_msg = ds_actuator_msgs::ServoCmd{};
  servo_msg.stamp = thruster_msg.stamp;

  for (auto i = 0; i < SentryThrusterAllocation::NUM_SERVOS; ++i)
  {
    const auto index = SentryThrusterAllocation::SERVO_START_INDEX + i;
    const auto actuator_index = static_cast<SentryThrusterAllocation::ActuatorInputIndex>(index);
    servo_msg.servo_name = sentry_control::to_string(actuator_index);
    try
    {
      servo_msg.cmd_radians = static_cast<float>(msg.inputs.at(index));
    }
    catch (std::out_of_range& e)
    {
      ROS_ERROR_STREAM("Caught out-of-range exception adding servo command with index: " << index);
      continue;
    }

    d->servos_pub_.at(i).publish(servo_msg);
  }
}
}
