/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_allocation.h"
#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_actuator_msgs/ServoCmd.h"

#include <ros/ros.h>
#include <gtest/gtest.h>
#include <future>

using namespace sentry_control;
using ThrusterParam = SentryAllocation::ThrusterParam;
using ServoParam = SentryAllocation::ServoParam;

class BaseThrusterAllocation : public SentryAllocation
{
public:
  explicit BaseThrusterAllocation() : SentryAllocation()
  {
  }

  uint64_t type() const noexcept override
  {
    return 0;
  }
};

class BaseAllocationTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    allocation_ = std::unique_ptr<BaseThrusterAllocation>(new BaseThrusterAllocation);
  }

  std::unique_ptr<BaseThrusterAllocation> allocation_;
};

TEST_F(BaseAllocationTest, set_current_coefficients)
{
  const auto expected = std::make_pair<double, double>(21.32, -32.1);
  allocation_->setCurrentCoefficients(expected.first, expected.second);
  const auto result = allocation_->currentCoefficients();
  EXPECT_FLOAT_EQ(expected.first, result.first);
  EXPECT_FLOAT_EQ(expected.second, result.second);
}

TEST_F(BaseAllocationTest, set_max_allowed_thrust)
{
  const auto expected = 0.5;
  allocation_->setMaxThrusterForce(expected);
  const auto result = allocation_->maxThrusterForce();
  EXPECT_FLOAT_EQ(expected, result);
}

TEST_F(BaseAllocationTest, set_max_speed)
{
  const auto expected = 0.5;
  allocation_->setMaxSpeed(expected);
  const auto result = allocation_->maxSpeed();
  EXPECT_FLOAT_EQ(expected, result);
}

TEST_F(BaseAllocationTest, set_reference_speed)
{
  auto expected = 0.5;
  allocation_->setReferenceSpeed(expected);
  auto result = allocation_->referenceSpeed();
  EXPECT_FLOAT_EQ(expected, result);

  // Negative values should be automatically set positive
  allocation_->setReferenceSpeed(-expected);
  result = allocation_->referenceSpeed();
  EXPECT_FLOAT_EQ(expected, result);

  // Values of magnitude greater than MaxSpeed should be clipped
  const auto max_speed = allocation_->maxSpeed();
  EXPECT_TRUE(max_speed > 0);
  allocation_->setReferenceSpeed(max_speed + 1);
  result = allocation_->referenceSpeed();
  EXPECT_FLOAT_EQ(max_speed, result);
}

TEST_F(BaseAllocationTest, current_from_force)
{
  // current equation is I = F / (alpha + beta * speed)

  // Make things easier...
  const auto alpha = 1.0;
  const auto beta = -1.0;

  allocation_->setCurrentCoefficients(alpha, beta);

  auto force = 1.0;
  auto speed = 0.5;
  auto expected = 2.0;
  auto result = allocation_->currentFromForce(force, speed);
  EXPECT_FLOAT_EQ(expected, result);

  // Shouldn't have div-by-zero errors, should produce infs, which then get clipped.
  force = 1.0;
  speed = 1.0;
  expected = std::numeric_limits<float>::quiet_NaN();
  result = allocation_->currentFromForce(force, speed);
  EXPECT_FLOAT_EQ(allocation_->maxThrusterCurrent(), result);

  // And from the other side...
  force = -1.0;
  speed = 1.0;
  expected = std::numeric_limits<float>::quiet_NaN();
  result = allocation_->currentFromForce(force, speed);
  EXPECT_FLOAT_EQ(-allocation_->maxThrusterCurrent(), result);

  // Speeds should be clipped by the max speed.
  allocation_->setMaxSpeed(0.5);
  force = 1.0;
  speed = 1.0;
  expected = 2.0;
  result = allocation_->currentFromForce(force, speed);
  EXPECT_FLOAT_EQ(expected, result);
}

TEST_F(BaseAllocationTest, default_actuator_limits)
{
  const auto expected_thrust_limit = 80;
  const auto expected_current_limit = 10;
  const auto expected_angle_limit = M_PI_2;

  EXPECT_FLOAT_EQ(expected_thrust_limit, allocation_->maxThrusterForce());
  EXPECT_FLOAT_EQ(expected_current_limit, allocation_->maxThrusterCurrent());
  EXPECT_FLOAT_EQ(expected_angle_limit, allocation_->maxServoAngle());
}

TEST_F(BaseAllocationTest, set_actuator_limits)
{
  const auto expected_thrust_limit = 100;
  const auto expected_current_limit = 5;
  const auto expected_angle_limit = M_PI_4;

  allocation_->setMaxThrusterForce(expected_thrust_limit);
  allocation_->setMaxThrusterCurrent(expected_current_limit);
  allocation_->setMaxServoAngle(expected_angle_limit);

  EXPECT_FLOAT_EQ(expected_thrust_limit, allocation_->maxThrusterForce());
  EXPECT_FLOAT_EQ(expected_current_limit, allocation_->maxThrusterCurrent());
  EXPECT_FLOAT_EQ(expected_angle_limit, allocation_->maxServoAngle());
}

TEST_F(BaseAllocationTest, actuator_clipping)
{
  const auto expected_thrust_limit = 1;
  const auto expected_current_limit = 1;
  const auto expected_angle_limit = M_PI_4;

  allocation_->setMaxThrusterForce(expected_thrust_limit);
  allocation_->setMaxThrusterCurrent(expected_current_limit);
  allocation_->setMaxServoAngle(expected_angle_limit);

  auto gains = SentryAllocation::Gains{.thruster_force_u = { 1, 1, 1, 1 },
                                       .thruster_force_w = { 0, 0, 0, 0 },
                                       .thruster_torque_w = { 0, 0, 0, 0 },
                                       .servo_force_w = { 1, 1 } };

  allocation_->setGains(gains);

  /// This should set force in = current out
  allocation_->setCurrentCoefficients(1, -1);
  allocation_->setReferenceSpeed(0);

  auto max_force = geometry_msgs::Wrench{};
  max_force.force.x = 10 * expected_thrust_limit;
  max_force.force.z = 10 * expected_angle_limit * allocation_->wingLiftCoefficient();
  const auto thruster_clipped_high = allocation_->calculateThrusterForces(max_force);
  const auto servo_clipped_high = allocation_->calculateServoAngles(max_force);

  auto min_force = max_force;
  min_force.force.x *= -1;
  min_force.force.z *= -1;

  const auto thruster_clipped_low = allocation_->calculateThrusterForces(min_force);
  const auto servo_clipped_low = allocation_->calculateServoAngles(min_force);

  for (auto i = 0; i < BaseThrusterAllocation::NUM_THRUSTERS; ++i)
  {
    EXPECT_FLOAT_EQ(expected_thrust_limit, thruster_clipped_high[i]);
    EXPECT_FLOAT_EQ(-expected_thrust_limit, thruster_clipped_low[i]);
  }

  for (auto i = 0; i < BaseThrusterAllocation::NUM_SERVOS; ++i)
  {
    EXPECT_FLOAT_EQ(expected_angle_limit, servo_clipped_high[i]);
    EXPECT_FLOAT_EQ(-expected_angle_limit, servo_clipped_low[i]);
  }

  auto nonclipping_force = geometry_msgs::Wrench{};
  nonclipping_force.force.x = allocation_->maxThrusterForce() / 2;
  nonclipping_force.force.z = allocation_->maxServoAngle() / 2 * allocation_->wingLiftCoefficient();

  const auto thruster_non_clipped = allocation_->calculateThrusterForces(nonclipping_force);
  const auto servo_non_clipped = allocation_->calculateServoAngles(nonclipping_force);

  for (auto i = 0; i < BaseThrusterAllocation::NUM_THRUSTERS; ++i)
  {
    EXPECT_FLOAT_EQ(nonclipping_force.force.x, thruster_non_clipped[i]);
  }

  for (auto i = 0; i < BaseThrusterAllocation::NUM_SERVOS; ++i)
  {
    EXPECT_FLOAT_EQ(allocation_->maxServoAngle() / 2, servo_non_clipped[i]);
  }
}

#if 0
TEST_F(BaseAllocationTest, actuator_gains)
{
  auto gains = allocation_->actuatorGains();
  for (auto i = 0; i < BaseThrusterAllocation::NUM_ACTUATOR_INPUTS; ++i)
  {
    gains[i].force.x = 6 * i;
    gains[i].force.y = 6 * i + 1;
    gains[i].force.z = 6 * i + 2;

    gains[i].torque.x = 6 * i + 3;
    gains[i].torque.y = 6 * i + 4;
    gains[i].torque.z = 6 * i + 5;
  }

  allocation_->setActuatorGains(gains);

  const auto expected = gains;
  gains = allocation_->actuatorGains();
  for (auto i = 0; i < BaseThrusterAllocation::NUM_ACTUATOR_INPUTS; ++i)
  {
    EXPECT_EQ(expected[i].force.x, gains[i].force.x);
    EXPECT_EQ(expected[i].force.y, gains[i].force.y);
    EXPECT_EQ(expected[i].force.z, gains[i].force.z);

    EXPECT_EQ(expected[i].torque.x, gains[i].torque.x);
    EXPECT_EQ(expected[i].torque.y, gains[i].torque.y);
    EXPECT_EQ(expected[i].torque.z, gains[i].torque.z);
  }
}
#endif

TEST_F(BaseAllocationTest, default_thruster_forces)
{
  auto gains = SentryAllocation::Gains{.thruster_force_u = { 1, 1, 1, 1 },
                                       .thruster_force_w = { 0, 0, 0, 0 },
                                       .thruster_torque_w = { 0.5, 0.5, 0, 0 },
                                       .servo_force_w = { 1, 1 } };
  allocation_->setGains(gains);

  auto wrench = geometry_msgs::Wrench{};
  wrench.force.x = -1;
  wrench.force.y = 2.5;
  wrench.force.z = -5;

  wrench.torque.x = 0;
  wrench.torque.y = -1;
  wrench.torque.z = 2;

  const auto forces = allocation_->calculateThrusterForces(wrench);
  const auto& wrench_gains = allocation_->thrusterWrenchGains();

  for (auto i = 0; i < BaseThrusterAllocation::NUM_THRUSTERS; ++i)
  {
    const auto expected = wrench_gains[i].force.x * wrench.force.x + wrench_gains[i].force.y * wrench.force.y +
                          wrench_gains[i].force.z * wrench.force.z +

                          wrench_gains[i].torque.x * wrench.torque.x + wrench_gains[i].torque.y * wrench.torque.y +
                          wrench_gains[i].torque.z * wrench.torque.z;

    EXPECT_EQ(expected, forces[i]);
  }
}

TEST_F(BaseAllocationTest, default_servo_angles)
{
  auto gains = SentryAllocation::Gains{.thruster_force_u = { 1, 1, 1, 1 },
                                       .thruster_force_w = { 0, 0, 0, 0 },
                                       .thruster_torque_w = { 0.5, 0.5, 0, 0 },
                                       .servo_force_w = { 1, 1 } };
  allocation_->setGains(gains);
  auto wrench = geometry_msgs::Wrench{};
  wrench.force.x = -1;
  wrench.force.y = 2.5;
  wrench.force.z = -5;

  wrench.torque.x = 0;
  wrench.torque.y = -1;
  wrench.torque.z = 2;

  const auto angles = allocation_->calculateServoAngles(wrench);
  const auto& wrench_gains = allocation_->servoWrenchGains();

  for (auto i = 0; i < BaseThrusterAllocation::NUM_SERVOS; ++i)
  {
    const auto expected = wrench_gains[i].force.x * wrench.force.x + wrench_gains[i].force.y * wrench.force.y +
                          wrench_gains[i].force.z * wrench.force.z +

                          wrench_gains[i].torque.x * wrench.torque.x + wrench_gains[i].torque.y * wrench.torque.y +
                          wrench_gains[i].torque.z * wrench.torque.z;

    EXPECT_EQ(expected, angles[i]);
  }
}

TEST_F(BaseAllocationTest, default_moment_arms)
{
  auto expected_arm = SentryAllocation::ThrusterParam{};
  expected_arm[BaseThrusterAllocation::PARAM_THRUSTER_PORT_FWD] = 0.76;
  expected_arm[BaseThrusterAllocation::PARAM_THRUSTER_STBD_FWD] = -0.76;
  expected_arm[BaseThrusterAllocation::PARAM_THRUSTER_PORT_AFT] = 0.76;
  expected_arm[BaseThrusterAllocation::PARAM_THRUSTER_STBD_AFT] = -0.76;

  const auto moment_arms = allocation_->momentArms();

  for (auto i = 0; i < BaseThrusterAllocation::NUM_THRUSTERS; ++i)
  {
    EXPECT_NEAR(expected_arm[i], moment_arms[i], 0.3);
  }
}

#if 0
TEST_F(BaseAllocationTest, set_gains)
{
  const auto expected =
      SentryAllocation::ThrusterParam{.port_fwd = 1.0, .stbd_fwd = 3.0, .port_aft = 2.0, .stbd_aft = 4.0 };

  allocation_->setThrusterForwardForceGains(expected);

  const auto result = allocation_->thrusterForwardForceGains();
  test_utils::compare_thruster_params(expected, result);
}
TEST_F(BaseAllocationTest, set_individual_gains)
{
  const auto expected =
      SentryAllocation::ThrusterParam{.port_fwd = 1.0, .stbd_fwd = 3.0, .port_aft = 2.0, .stbd_aft = 4.0 };

  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_FWD, expected.port_fwd);
  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_AFT, expected.port_aft);
  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_FWD, expected.stbd_fwd);
  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_AFT, expected.stbd_aft);

  auto result = ThrusterParam{};

  result.port_fwd = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_FWD);
  result.port_aft = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_AFT);
  result.stbd_fwd = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_FWD);
  result.stbd_aft = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_AFT);

  test_utils::compare_thruster_params(expected, result);
}

TEST_F(BaseAllocationTest, set_moment_arms)
{
  const auto expected =
      SentryAllocation::ThrusterParam{.port_fwd = 1.0, .stbd_fwd = 3.0, .port_aft = 2.0, .stbd_aft = 4.0 };

  allocation_->setThrusterForwardForceGains(expected);

  const auto result = allocation_->thrusterForwardForceGains();
  test_utils::compare_thruster_params(expected, result);
}

TEST_F(BaseAllocationTest, set_individual_moment_arms)
{
  const auto expected =
      SentryAllocation::ThrusterParam{.port_fwd = 1.0, .stbd_fwd = 3.0, .port_aft = 2.0, .stbd_aft = 4.0 };

  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_FWD, expected.port_fwd);
  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_AFT, expected.port_aft);
  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_FWD, expected.stbd_fwd);
  allocation_->setThrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_AFT, expected.stbd_aft);

  auto result = ThrusterParam{};

  result.port_fwd = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_FWD);
  result.port_aft = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::PORT_AFT);
  result.stbd_fwd = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_FWD);
  result.stbd_aft = allocation_->thrusterForwardForceGain(SentryAllocation::ThrusterIndex::STBD_AFT);

  test_utils::compare_thruster_params(expected, result);
}

TEST_F(BaseAllocationTest, set_wing_lift_coefficient)
{
  const auto expected = 0.5;
  allocation_->setWingLiftForce(expected);
  const auto result = allocation_->wingLiftForce();

  EXPECT_FLOAT_EQ(expected, result);
}


class BaseAllocationCommandTest : public BaseAllocationTest
{
protected:
  using ThrusterResponse = boost::shared_ptr<const ds_actuator_msgs::ThrusterCmd>;
  using ServoResponse = boost::shared_ptr<const ds_actuator_msgs::ServoCmd>;

  using ThrusterIndex = SentryAllocation::ThrusterIndex;
  using ServoIndex = SentryAllocation::ServoIndex;

  const static auto NUM_THRUSTERS = SentryAllocation::NUM_THRUSTERS;
  const static auto NUM_SERVOS = SentryAllocation::NUM_SERVOS;

  void SetUp() override
  {
    BaseAllocationTest::SetUp();
  }

  std::array<std::future<ThrusterResponse>, NUM_THRUSTERS>
  sendThrusterCommands(const std::array<double, NUM_THRUSTERS>& forces, const std::array<double, NUM_THRUSTERS>& speeds,
                       ros::Duration timeout = ros::Duration(2.0))
  {
    auto wait_for_thruster = [](ThrusterIndex index, ros::Duration timeout) {
      const auto topic = to_string(index);
      return ros::topic::waitForMessage<ds_actuator_msgs::ThrusterCmd>(topic, timeout);
    };

    auto futures = std::array<std::future<ThrusterResponse>, NUM_THRUSTERS>{};
    for (auto index = 0; index < NUM_THRUSTERS; ++index)
    {
      futures[index] = std::async(std::launch::async, wait_for_thruster, static_cast<ThrusterIndex>(index), timeout);
    }

    // This is important!  We need to let the subscribers actually finish connecting.. so wait here for a moment
    // before sending the commands.
    std::this_thread::sleep_for(std::chrono::seconds(1));

    for (auto index = 0; index < NUM_THRUSTERS; ++index)
    {
      allocation_->sendThrusterCommand_(static_cast<ThrusterIndex>(index), forces[index], speeds[index]);
    }

    return futures;
  };

  std::array<std::future<ServoResponse>, NUM_SERVOS> sendServoCommands(const std::array<double, NUM_SERVOS>& angles,
                                                                       ros::Duration timeout = ros::Duration(2.0))
  {
    auto wait_for_servo = [](ServoIndex index, ros::Duration timeout) {
      const auto topic = to_string(index);
      return ros::topic::waitForMessage<ds_actuator_msgs::ServoCmd>(topic, timeout);
    };

    auto futures = std::array<std::future<ServoResponse>, NUM_SERVOS>{};
    for (auto index = 0; index < NUM_SERVOS; ++index)
    {
      futures[index] = std::async(std::launch::async, wait_for_servo, static_cast<ServoIndex>(index), timeout);
    }

    // This is important!  We need to let the subscribers actually finish connecting.. so wait here for a moment
    // before sending the commands.
    std::this_thread::sleep_for(std::chrono::seconds(1));

    for (auto index = 0; index < NUM_SERVOS; ++index)
    {
      allocation_->sendServoCommand_(static_cast<ServoIndex>(index), angles[index]);
    }

    return futures;
  };
};

TEST_F(BaseAllocationCommandTest, send_thruster_command)
{
  const auto alpha = 1.0;
  const auto beta = -1.0;
  allocation_->setCurrentCoefficients(alpha, beta);

  const auto base_speed = 0.5;

  auto speeds = std::array<double, NUM_THRUSTERS>{};
  for (auto index = 0; index < NUM_THRUSTERS; ++index)
  {
    speeds[index] = base_speed + 0.1 * index;
  }

  const auto forces = std::array<double, NUM_THRUSTERS>{ 1.0 };

  auto expected = std::array<ds_actuator_msgs::ThrusterCmd, NUM_THRUSTERS>{};
  for (auto index = 0; index < NUM_THRUSTERS; ++index)
  {
    expected[index].stamp = ros::Time::now();
    expected[index].cmd_value = static_cast<float>(forces[index] / (alpha + beta * speeds[index]));
    expected[index].thruster_name = to_string(static_cast<ThrusterIndex>(index));
  }

  allocation_->setEnabled(true);
  auto futures = sendThrusterCommands(forces, speeds);

  for (auto index = 0; index < NUM_THRUSTERS; ++index)
  {
    auto result = futures[index].get();
    if (!result)
    {
      FAIL() << "No command received on channel " << to_string(static_cast<ThrusterIndex>(index));
    }

    EXPECT_LT(result->stamp - expected[index].stamp, ros::Duration(2));
    EXPECT_STREQ(expected[index].thruster_name.data(), result->thruster_name.data());
    EXPECT_FLOAT_EQ(expected[index].cmd_value, result->cmd_value);
  }
}

TEST_F(BaseAllocationCommandTest, send_thruster_commands_while_disabled)
{
  const auto speeds = std::array<double, NUM_THRUSTERS>{ 0.5 };
  const auto forces = std::array<double, NUM_THRUSTERS>{ 0 };

  allocation_->setEnabled(false);
  auto futures = sendThrusterCommands(forces, speeds);

  for (auto index = 0; index < NUM_THRUSTERS; ++index)
  {
    auto result = futures[index].get();
    if (result)
    {
      FAIL() << "Received command while disabled on channel " << to_string(static_cast<ThrusterIndex>(index));
    }
  }
}

TEST_F(BaseAllocationCommandTest, send_servo_commands)
{
  const auto angles = std::array<double, NUM_SERVOS>{ 0.5, -0.5 };
  auto expected = std::array<ds_actuator_msgs::ServoCmd, NUM_SERVOS>{};
  for (auto index = 0; index < NUM_SERVOS; ++index)
  {
    expected[index].stamp = ros::Time::now();
    expected[index].cmd_radians = static_cast<float>(angles[index]);
    expected[index].servo_name = to_string(static_cast<ServoIndex>(index));
  }

  allocation_->setEnabled(true);
  auto futures = sendServoCommands(angles);

  for (auto index = 0; index < NUM_SERVOS; ++index)
  {
    auto result = futures[index].get();
    if (!result)
    {
      FAIL() << "No command received on channel " << to_string(static_cast<ServoIndex>(index));
    }

    EXPECT_LT(result->stamp - expected[index].stamp, ros::Duration(2));
    EXPECT_STREQ(expected[index].servo_name.data(), result->servo_name.data());
    EXPECT_FLOAT_EQ(expected[index].cmd_radians, result->cmd_radians);
  }
}

TEST_F(BaseAllocationCommandTest, send_servo_commands_while_disabled)
{
  const auto angles = std::array<double, NUM_SERVOS>{ 0.5, -0.5 };
  auto expected = std::array<ds_actuator_msgs::ServoCmd, NUM_SERVOS>{};
  for (auto index = 0; index < NUM_SERVOS; ++index)
  {
    expected[index].stamp = ros::Time::now();
    expected[index].cmd_radians = static_cast<float>(angles[index]);
    expected[index].servo_name = to_string(static_cast<ServoIndex>(index));
  }

  auto futures = sendServoCommands(angles);

  for (auto index = 0; index < NUM_SERVOS; ++index)
  {
    auto result = futures[index].get();
    if (result)
    {
      FAIL() << "Received command while disabled on channel " << to_string(static_cast<ServoIndex>(index));
    }
  }
}
#endif

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  return ret;
};
