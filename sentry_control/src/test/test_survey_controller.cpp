/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_survey_controller.h"
#include "sentry_control/sentry_allocation.h"
#include "ds_control/pid_heading.h"

#include "ds_actuator_msgs/ThrusterCmd.h"
#include "ds_actuator_msgs/ServoCmd.h"
#include "ds_nav_msgs/ModelState.h"

#include <gtest/gtest.h>
#include <future>
#include <sentry_sim_core/sentry_sim_core.h>

using namespace sentry_control;

class SurveyControllerTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    controller_ = std::unique_ptr<SentrySurveyController>(new SentrySurveyController);
    controller_->setup();
    alloc_ = std::unique_ptr<SentryAllocation>(new SentryAllocation(FLIGHT0_ALLOCATION_GAINS));
    alloc_->setup();
  }

  void TearDown() override
  {
    controller_.reset();
    alloc_.reset();
  }

  ds_nav_msgs::ModelState stepModel(ds_nav_msgs::AggregatedState state, geometry_msgs::Wrench wrench, double dt)
  {
    const auto thruster_forces = alloc_->calculateThrusterForces(wrench);
    const auto servo_angles = alloc_->calculateServoAngles(wrench);

    auto thrusters = std::vector<ds_actuator_msgs::ThrusterCmd>(4);
    auto servos = std::vector<ds_actuator_msgs::ServoCmd>(2);

    thrusters[sentry_sim::FWD_PORT].cmd_newtons =
        static_cast<float>(thruster_forces[SentryAllocation::PARAM_THRUSTER_PORT_FWD]);
    thrusters[sentry_sim::FWD_STBD].cmd_newtons =
        static_cast<float>(thruster_forces[SentryAllocation::PARAM_THRUSTER_STBD_FWD]);
    thrusters[sentry_sim::AFT_PORT].cmd_newtons =
        static_cast<float>(thruster_forces[SentryAllocation::PARAM_THRUSTER_PORT_AFT]);
    thrusters[sentry_sim::AFT_STBD].cmd_newtons =
        static_cast<float>(thruster_forces[SentryAllocation::PARAM_THRUSTER_STBD_AFT]);

    servos[sentry_sim::SERVO_AFT].cmd_radians = static_cast<float>(servo_angles[SentryAllocation::PARAM_SERVO_AFT]);

    servos[sentry_sim::SERVO_FWD].cmd_radians = static_cast<float>(servo_angles[SentryAllocation::PARAM_SERVO_FWD]);

    auto sim = sentry_sim::SentrySimCore{};
    auto buoyancy = ds_nav_msgs::Buoyancy{};

    return sim.runVehicleModel(std::move(state), thrusters, servos, buoyancy, dt);
  }
  std::unique_ptr<SentrySurveyController> controller_;
  std::unique_ptr<SentryAllocation> alloc_;
};

TEST_F(SurveyControllerTest, test_closed_loop_heading)
{
  auto state = ds_nav_msgs::AggregatedState{};
  state.header.stamp = ros::Time::now();
  state.heading.value = 0;
  state.heading.valid = true;
  state.surge_u.value = 0;
  state.surge_u.valid = true;
  state.down.value = 0;
  state.down.valid = true;

  auto model_state = ds_nav_msgs::ModelState{};
  model_state.header = state.header;
  model_state.ds_header = state.ds_header;

  const auto desired_heading_deg = 90;
  auto reference = state;
  reference.heading.value = desired_heading_deg * M_PI / 180;

  controller_->setEnabled(true);
  // controller_->setAutoHeadingEnabled(true);

  const auto num_iterations = 1000;
  const auto dt = ros::Duration{ 0.25 };
  for (auto i = 0; i <= num_iterations; ++i)
  {
    auto wrench = controller_->calculateForces(reference, state, dt);
    auto model = stepModel(state, std::move(wrench.wrench), dt.toSec());

    state.header = model.header;
    state.ds_header = model.ds_header;

    state.heading.valid = true;
    state.heading.value = model.heading;

    state.down.valid = true;
    state.down.value = model.down;

    state.easting.valid = true;
    state.easting.value = model.easting;

    state.northing.valid = true;
    state.northing.value = model.northing;

    state.pitch.valid = true;
    state.pitch.value = model.pitch;

    state.roll.valid = true;
    state.roll.value = model.roll;

    state.surge_u.valid = true;
    state.surge_u.value = model.surge_u;

    state.heave_w.valid = true;
    state.heave_w.value = model.heave_w;

    state.sway_v.valid = true;
    state.sway_v.value = model.sway_v;

    state.r.valid = true;
    state.r.value = model.r;

    state.p.valid = true;
    state.p.value = model.p;

    state.q.valid = true;
    state.q.value = model.q;

    if ((i % 50) == 0)
    {
      ROS_INFO("Iteration % 4d, % 6.2fs:  Heading: % 6.2f, error: % 6.2f", i, i * dt.toSec(),
               state.heading.value * 180 / M_PI, desired_heading_deg - state.heading.value * 180 / M_PI);
    }
  }

  EXPECT_NEAR(state.heading.value * 180 / M_PI, desired_heading_deg, 2);
}

TEST_F(SurveyControllerTest, test_closed_loop_speed)
{
  // controller_->setHeadingRateDeadband(10);

  auto state = ds_nav_msgs::AggregatedState{};
  state.header.stamp = ros::Time::now();
  state.surge_u.value = 0;
  state.surge_u.valid = true;
  state.heading.value = 0;
  state.heading.valid = true;
  state.down.value = 0;
  state.down.valid = true;

  auto model_state = ds_nav_msgs::ModelState{};
  model_state.header = state.header;
  model_state.ds_header = state.ds_header;

  const auto desired_speed = 1.0;
  auto reference = state;
  reference.surge_u.value = desired_speed;

  controller_->setEnabled(true);
  // controller_->setAutoHeadingEnabled(true);

  const auto num_iterations = 250;
  const auto dt = ros::Duration{ 0.25 };
  for (auto i = 0; i <= num_iterations; ++i)
  {
    auto wrench = controller_->calculateForces(reference, state, dt);
    auto model = stepModel(state, std::move(wrench.wrench), dt.toSec());

    state.header = model.header;
    state.ds_header = model.ds_header;

    state.heading.valid = true;
    state.heading.value = model.heading;

    state.down.valid = true;
    state.down.value = model.down;

    state.easting.valid = true;
    state.easting.value = model.easting;

    state.northing.valid = true;
    state.northing.value = model.northing;

    state.pitch.valid = true;
    state.pitch.value = model.pitch;

    state.roll.valid = true;
    state.roll.value = model.roll;

    state.surge_u.valid = true;
    state.surge_u.value = model.surge_u;

    state.heave_w.valid = true;
    state.heave_w.value = model.heave_w;

    state.sway_v.valid = true;
    state.sway_v.value = model.sway_v;

    state.r.valid = true;
    state.r.value = model.r;

    state.p.valid = true;
    state.p.value = model.p;

    state.q.valid = true;
    state.q.value = model.q;

    if ((i % 10) == 0)
    {
      ROS_INFO("Iteration % 4d, % 6.2fs:  Speed: % 6.2f, error: % 6.2f", i, i * dt.toSec(), state.surge_u.value,
               desired_speed - state.surge_u.value);
    }
  }
  EXPECT_NEAR(state.surge_u.value, desired_speed, 0.1);
}

TEST_F(SurveyControllerTest, test_closed_loop_depth)
{
  // controller_->setHeadingRateDeadband(10);

  const auto desired_depth = 50;
  const auto desired_fwd_vel = 1.0;

  auto state = ds_nav_msgs::AggregatedState{};
  state.header.stamp = ros::Time::now();
  state.heading.value = 0;
  state.heading.valid = true;
  state.surge_u.value = desired_fwd_vel;
  state.surge_u.valid = true;
  state.down.value = 45;
  state.down.valid = true;

  auto model_state = ds_nav_msgs::ModelState{};
  model_state.header = state.header;
  model_state.ds_header = state.ds_header;

  auto reference = state;
  reference.down.value = desired_depth;
  reference.surge_u.value = desired_fwd_vel;

  controller_->setEnabled(true);
  // controller_->setAutoHeadingEnabled(true);

  const auto num_iterations = 150;
  const auto dt = ros::Duration{ 0.1 };
  for (auto i = 0; i <= num_iterations; ++i)
  {
    auto wrench = controller_->calculateForces(reference, state, dt);
    auto model = stepModel(state, std::move(wrench.wrench), dt.toSec());

    state.header = model.header;
    state.ds_header = model.ds_header;

    state.heading.valid = true;
    state.heading.value = model.heading;

    state.down.valid = true;
    state.down.value = model.down;

    state.easting.valid = true;
    state.easting.value = model.easting;

    state.northing.valid = true;
    state.northing.value = model.northing;

    state.pitch.valid = true;
    state.pitch.value = model.pitch;

    state.roll.valid = true;
    state.roll.value = model.roll;

    state.surge_u.valid = true;
    state.surge_u.value = model.surge_u;

    state.heave_w.valid = true;
    state.heave_w.value = model.heave_w;

    state.sway_v.valid = true;
    state.sway_v.value = model.sway_v;

    state.r.valid = true;
    state.r.value = model.r;

    state.p.valid = true;
    state.p.value = model.p;

    state.q.valid = true;
    state.q.value = model.q;

    if ((i % 10) == 0)
    {
      ROS_INFO("Itreation % 4d, % 6.2fs:  Depth: % 6.2f, Depth rate: %6.2f error: % 6.2f", i, i * dt.toSec(),
               state.down.value, state.heave_w.value, desired_depth - state.down.value);
    }
  }

  EXPECT_NEAR(state.down.value, desired_depth, 1);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "survey");
  ros::NodeHandle nh;
  auto ret = RUN_ALL_TESTS();
  return ret;
};
