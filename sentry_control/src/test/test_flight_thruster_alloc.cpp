/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_allocation.h"
#include <ros/ros.h>
#include <gtest/gtest.h>

using namespace sentry_control;
using ThrusterParam = SentryAllocation::ThrusterParam;
using ServoParam = SentryAllocation::ServoParam;

class AllocationTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    allocation_ = std::unique_ptr<SentryAllocation>(new SentryAllocation);
  }

  std::unique_ptr<SentryAllocation> allocation_;
};

TEST_F(AllocationTest, preset_flight1_gains)
{
  allocation_ = std::unique_ptr<SentryAllocation>(new SentryAllocation(FLIGHT1_ALLOCATION_GAINS));

  auto expected_fwd = SentryAllocation::ThrusterParam{};
  expected_fwd.fill(0);
  expected_fwd[SentryAllocation::PARAM_THRUSTER_PORT_FWD] = 0.3;
  expected_fwd[SentryAllocation::PARAM_THRUSTER_STBD_FWD] = 0.3;
  expected_fwd[SentryAllocation::PARAM_THRUSTER_PORT_AFT] = 0.2;
  expected_fwd[SentryAllocation::PARAM_THRUSTER_STBD_AFT] = 0.2;

  auto expected_vert = SentryAllocation::ThrusterParam{};
  expected_vert.fill(0);

  const auto moment_arms = allocation_->momentArms();
  const auto heading_gains = allocation_->headingGain();

  const auto& thruster_gains = allocation_->thrusterWrenchGains();
  for (auto i = 0; i < SentryAllocation::NUM_THRUSTERS; ++i)
  {
    auto& gain = thruster_gains[i];
    EXPECT_FLOAT_EQ(expected_fwd[i], gain.force.x);
    EXPECT_FLOAT_EQ(0, gain.force.y);
    EXPECT_FLOAT_EQ(expected_vert[i], gain.force.z);

    EXPECT_FLOAT_EQ(0, gain.torque.x);
    EXPECT_FLOAT_EQ(0, gain.torque.y);
    EXPECT_FLOAT_EQ(heading_gains[i] / moment_arms[i], gain.torque.z);
  }

  const auto servo_gains = allocation_->servoGain();
  const auto wing_lift = allocation_->wingLiftCoefficient();

  const auto& servo_wrenches = allocation_->servoWrenchGains();

  for (auto i = 0; i < SentryAllocation::NUM_SERVOS; ++i)
  {
    auto& gain = servo_wrenches[i];
    EXPECT_FLOAT_EQ(0, gain.force.x);
    EXPECT_FLOAT_EQ(0, gain.force.y);
    EXPECT_FLOAT_EQ(servo_gains[i] / wing_lift, gain.force.z);

    EXPECT_FLOAT_EQ(0, gain.torque.x);
    EXPECT_FLOAT_EQ(0, gain.torque.y);
    EXPECT_FLOAT_EQ(0, gain.torque.z);
  }
}

TEST_F(AllocationTest, preset_flight0_gains)
{
  allocation_ = std::unique_ptr<SentryAllocation>(new SentryAllocation(FLIGHT0_ALLOCATION_GAINS));

  auto expected_fwd = SentryAllocation::ThrusterParam{};
  expected_fwd.fill(0);
  expected_fwd[SentryAllocation::PARAM_THRUSTER_PORT_FWD] = 0.0;
  expected_fwd[SentryAllocation::PARAM_THRUSTER_STBD_FWD] = 0.0;
  expected_fwd[SentryAllocation::PARAM_THRUSTER_PORT_AFT] = 0.5;
  expected_fwd[SentryAllocation::PARAM_THRUSTER_STBD_AFT] = 0.5;

  const auto moment_arms = allocation_->momentArms();
  const auto heading_gains = allocation_->headingGain();

  const auto& thruster_gains = allocation_->thrusterWrenchGains();
  for (auto i = 0; i < SentryAllocation::NUM_THRUSTERS; ++i)
  {
    auto& gain = thruster_gains[i];
    EXPECT_FLOAT_EQ(expected_fwd[i], gain.force.x);
    EXPECT_FLOAT_EQ(0, gain.force.y);
    EXPECT_FLOAT_EQ(0, gain.force.z);

    EXPECT_FLOAT_EQ(0, gain.torque.x);
    EXPECT_FLOAT_EQ(0, gain.torque.y);
    EXPECT_FLOAT_EQ(heading_gains[i] / moment_arms[i], gain.torque.z);
  }

  const auto servo_gains = allocation_->servoGain();
  const auto wing_lift = allocation_->wingLiftCoefficient();

  const auto& servo_wrenches = allocation_->servoWrenchGains();
  for (auto i = 0; i < SentryAllocation::NUM_SERVOS; ++i)
  {
    auto& gain = servo_wrenches[i];
    EXPECT_FLOAT_EQ(0, gain.force.x);
    EXPECT_FLOAT_EQ(0, gain.force.y);
    EXPECT_FLOAT_EQ(servo_gains[i] / wing_lift, gain.force.z);

    EXPECT_FLOAT_EQ(0, gain.torque.x);
    EXPECT_FLOAT_EQ(0, gain.torque.y);
    EXPECT_FLOAT_EQ(0, gain.torque.z);
  }
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  return ret;
};
