/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_control/sentry_allocation.h"
#include "ds_util/dynamics.h"

#include <ros/param.h>
#include <gtest/gtest.h>
#include <random>
#include <tuple>

using namespace sentry_control;

class BaseThrusterAllocation : public SentryAllocation
{
public:
  explicit BaseThrusterAllocation() : SentryAllocation()
  {
  }

  uint64_t type() const noexcept override
  {
    return 0;
  }
};

class BaseAllocationParameterTest : public ::testing::Test
{
protected:
  void SetUp() override
  {
    gen = std::mt19937{ rd() };
    dis = std::uniform_real_distribution<>(-100.0, 100.0);
    allocation_ = std::unique_ptr<BaseThrusterAllocation>(new BaseThrusterAllocation);
  }

  std::unique_ptr<BaseThrusterAllocation> allocation_;
  std::random_device rd;  // Will be used to obtain a seed for the random number engine
  std::mt19937 gen;       // Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis;
};

TEST_F(BaseAllocationParameterTest, read_limit_parameters)
{
  const auto max_thruster_current = std::abs(dis(gen));
  const auto max_thruster_force = std::abs(dis(gen));

  ros::param::set("max_thruster_current", max_thruster_current);
  ros::param::set("max_thruster_force", max_thruster_force);

  allocation_->setup();

  EXPECT_FLOAT_EQ(max_thruster_current, allocation_->maxThrusterCurrent());
  EXPECT_FLOAT_EQ(max_thruster_force, allocation_->maxThrusterForce());
}

TEST_F(BaseAllocationParameterTest, read_current_transform_parameters)
{
  const auto alpha = dis(gen);
  const auto beta = dis(gen);

  ros::param::set("force_to_amps_transform/alpha", alpha);
  ros::param::set("force_to_amps_transform/beta", beta);

  allocation_->setup();

  const auto current_coefficients = allocation_->currentCoefficients();
  EXPECT_FLOAT_EQ(alpha, current_coefficients.first);
  EXPECT_FLOAT_EQ(beta, current_coefficients.second);
}

TEST_F(BaseAllocationParameterTest, read_thruster_spin_parameters)
{
  const auto current_aligned_with_force = std::array<bool, 4>{ false, true, true, false };
  ros::param::set("pos_force_is_pos_current/thruster_fwd_port",
                  current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_PORT_FWD]);

  ros::param::set("pos_force_is_pos_current/thruster_fwd_stbd",
                  current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_STBD_FWD]);

  ros::param::set("pos_force_is_pos_current/thruster_aft_port",
                  current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_PORT_AFT]);

  ros::param::set("pos_force_is_pos_current/thruster_aft_stbd",
                  current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_STBD_AFT]);

  allocation_->setup();

  const auto actual_current_alignment = allocation_->posForceIsPosCurrent();

  EXPECT_EQ(current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_PORT_FWD],
            actual_current_alignment[SentryAllocation::PARAM_THRUSTER_PORT_FWD]);

  EXPECT_EQ(current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_STBD_FWD],
            actual_current_alignment[SentryAllocation::PARAM_THRUSTER_STBD_FWD]);

  EXPECT_EQ(current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_PORT_AFT],
            actual_current_alignment[SentryAllocation::PARAM_THRUSTER_PORT_AFT]);

  EXPECT_EQ(current_aligned_with_force[SentryAllocation::PARAM_THRUSTER_STBD_AFT],
            actual_current_alignment[SentryAllocation::PARAM_THRUSTER_STBD_AFT]);
}

#if 0
TEST_F(BaseAllocationParameterTest, read_wing_dynamics)
{
  const auto area = dis(gen);
  const auto density = dis(gen);
  ros::param::set("CS_Area", area);
  ros::param::set("CS_Rho", density);

  const auto cl0 = dis(gen);
  const auto cl_low = dis(gen);
  const auto cl_high = dis(gen);
  ros::param::set("CS_Cl0", cl0);
  ros::param::set("CS_Cl1", cl_low);
  ros::param::set("CS_Cl2", cl_high);

  const auto cl_angle_low = dis(gen);
  const auto cl_angle_high = dis(gen);
  ros::param::set("CS_alpha1", cl_angle_low);
  ros::param::set("CS_alpha2", cl_angle_high);

  const auto cd0 = dis(gen);
  const auto cd1 = dis(gen);
  const auto cd2 = dis(gen);
  ros::param::set("CS_Cd0", cd0);
  ros::param::set("CS_Cd1", cd1);
  ros::param::set("CS_Cd2", cd2);

  auto nh = ros::NodeHandle();
  allocation_->setupFromParameterServer(nh);

  auto wing = allocation_->wingDynamics();
  ASSERT_TRUE(wing.get() != nullptr);

  EXPECT_FLOAT_EQ(area, wing->wingArea());
  EXPECT_FLOAT_EQ(density, wing->fluidDensity());

  auto result_cl0 = 0.0;
  auto result_cl_low = 0.0;
  auto result_cl_high = 0.0;
  std::tie(result_cl0, result_cl_low, result_cl_high) = wing->liftCoefficients();

  EXPECT_FLOAT_EQ(cl0, result_cl0);
  EXPECT_FLOAT_EQ(cl_low, result_cl_low);
  EXPECT_FLOAT_EQ(cl_high, result_cl_high);

  auto result_cl_angle_low = 0.0;
  auto result_cl_angle_high = 0.0;
  std::tie(result_cl_angle_low, result_cl_angle_high) = wing->liftAngleRegions();

  auto result_cd0 = 0.0;
  auto result_cd1 = 0.0;
  auto result_cd2 = 0.0;
  std::tie(result_cd0, result_cd1, result_cd2) = wing->dragCoefficients();

  EXPECT_FLOAT_EQ(cd0, result_cd0);
  EXPECT_FLOAT_EQ(cd1, result_cd1);
  EXPECT_FLOAT_EQ(cd2, result_cd2);
}
#endif

TEST_F(BaseAllocationParameterTest, read_urdf_model)
{
  // std::random_device rd;  //Will be used to obtain a seed for the random number engine
  // std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  // std::uniform_real_distribution<> dis(-100., 100.0);

  allocation_->setup();

  const auto moment_arms = allocation_->momentArms();
  EXPECT_NEAR(moment_arms[SentryAllocation::PARAM_THRUSTER_PORT_FWD],
              DEFAULT_SENTRY_MOMENT_ARMS[SentryAllocation::PARAM_THRUSTER_PORT_FWD], 0.001);

  EXPECT_NEAR(moment_arms[SentryAllocation::PARAM_THRUSTER_STBD_FWD],
              DEFAULT_SENTRY_MOMENT_ARMS[SentryAllocation::PARAM_THRUSTER_STBD_FWD], 0.001);

  EXPECT_NEAR(moment_arms[SentryAllocation::PARAM_THRUSTER_PORT_AFT],
              DEFAULT_SENTRY_MOMENT_ARMS[SentryAllocation::PARAM_THRUSTER_PORT_AFT], 0.001);

  EXPECT_NEAR(moment_arms[SentryAllocation::PARAM_THRUSTER_STBD_AFT],
              DEFAULT_SENTRY_MOMENT_ARMS[SentryAllocation::PARAM_THRUSTER_STBD_AFT], 0.001);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "thruster_allocation_paramtest");
  ros::NodeHandle nh;
  auto ret = RUN_ALL_TESTS();
  return ret;
};
