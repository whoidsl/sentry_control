/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GOAL_TIMED_JOY_GOAL_TIMED_JOY_H
#define GOAL_TIMED_JOY_GOAL_TIMED_JOY_H

#include "ds_control/joystick_base.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "sentry_msgs/SentryJoystickEnum.h"
#include "sentry_msgs/TimedJoystick.h"

namespace goal_timed_joy
{

struct GoalTimedJoyPrivate;

/// @brief Timed Joystick Goal
///
/// The Timed Joystick Goal will periodically publish a desired AggregatedState joystick
/// message for a set duration of time.  This may be useful for control cases where
/// communications between vehicle and operator is difficult or extremely low-bandwidth.
///
/// Two virtual functions: `preActivationHook` and `postTimeoutHook` are called before the joystick
/// starts publishing messages and after the joystick stops, respectively.  These may be used
/// to save and restore existing vehicle state.
class GoalTimedJoy : public ds_control::JoystickBase
{
  DS_DECLARE_PRIVATE(GoalTimedJoy)

public:
  GoalTimedJoy();
  GoalTimedJoy(int argc, char* argv[], const std::string& name);
  ~GoalTimedJoy() override;
  DS_DISABLE_COPY(GoalTimedJoy)

  uint64_t type() const noexcept override
  {
    return sentry_msgs::SentryJoystickEnum::TIMED;
  }

  /// @brief Get the aggregated state message to be published
  ///
  /// \return
  ds_nav_msgs::AggregatedState joystickState() const noexcept;

  /// @brief Set the duration that the joystick message will be published.
  ///
  /// If `start` is true the message goal will become active immediately.
  ///
  /// \param timeout
  /// \param start
  void setDuration(ros::Duration timeout);

  /// @brief Get the current active duration of the joystick.
  ///
  /// \return
  ros::Duration duration() const noexcept;

  /// @brief Time remaining until the joystick stops publishing.
  ///
  /// \return
  ros::Duration remaining() noexcept;

  /// @brief Set the update period to publish joystick messages when active.
  ///
  /// \param period
  void setUpdatePeriod(ros::Duration period);

  /// @brief Get the period at which joystick messages are published when active.
  ///
  /// \return
  ros::Duration updatePeriod() const noexcept;

  /// @brief Returns true if the goal is actively publishing joystick messages.
  ///
  /// \return
  bool isActive();

  /// @brief Start publishing joystick messages.
  ///
  /// Starts publishing joystick messages (set using `setJoystickState`) for the
  /// set timeout period (`setTimeout`) at the set update period (`setUpdatePeriod`)
  ///
  ///
  /// Returns true if both the timeout period and update period are valid (>0).  False
  /// otherwise.
  /// \return
  bool start(const sentry_msgs::TimedJoystick& msg);

  /// @brief Stop publishing joystick messages
  void stop();

protected:
  void setupTimers() override;
  void setupPublishers() override;
  void setupServices() override;

private:
  void publishTimerCallback(const ros::TimerEvent&);

  std::unique_ptr<GoalTimedJoyPrivate> d_ptr_;
};
}
#endif  // GOAL_TIMED_JOY_GOAL_TIMED_JOY_H
