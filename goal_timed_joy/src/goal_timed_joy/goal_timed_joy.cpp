/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "goal_timed_joy/goal_timed_joy.h"
#include "sentry_msgs/TimedJoystickService.h"
#include "sentry_msgs/TimedJoystick.h"

namespace goal_timed_joy
{
struct GoalTimedJoyPrivate
{
  ros::Duration timeout_period_;
  ros::Time start_time_;

  ros::Duration publish_period_ = ros::Duration(1.0);
  ros::Timer publish_timer_;

  ds_nav_msgs::AggregatedState state_;

  bool is_active_ = false;

  ros::Publisher timed_joystick_pub_;
  ros::ServiceServer timed_joystick_srv_;

  int orig_controller_;
  int orig_allocation_;
  int orig_joystick_;

  int desired_allocation_;
  int desired_controller_;
  bool restore_allocation_;
  bool restore_controller_;

  bool startAsService(GoalTimedJoy* base, const sentry_msgs::TimedJoystickService::Request& req,
                      sentry_msgs::TimedJoystickService::Response& resp)
  {
    return base->start(req.joystick);
  }
};

GoalTimedJoy::GoalTimedJoy()
  : ds_control::JoystickBase(), d_ptr_(std::unique_ptr<GoalTimedJoyPrivate>(new GoalTimedJoyPrivate))
{
}

GoalTimedJoy::GoalTimedJoy(int argc, char** argv, const std::string& name)
  : ds_control::JoystickBase(argc, argv, name), d_ptr_(std::unique_ptr<GoalTimedJoyPrivate>(new GoalTimedJoyPrivate))
{
}

GoalTimedJoy::~GoalTimedJoy() = default;

ds_nav_msgs::AggregatedState GoalTimedJoy::joystickState() const noexcept
{
  const DS_D(GoalTimedJoy);
  return d->state_;
}

bool GoalTimedJoy::isActive()
{
  DS_D(GoalTimedJoy);
  return d->is_active_;
}

void GoalTimedJoy::setDuration(ros::Duration timeout)
{
  if (timeout.toSec() <= 0)
  {
    ROS_ERROR_STREAM("Invalid timeout (" << std::fixed << timeout.toSec() << ").  Must be >0.");
    return;
  }

  DS_D(GoalTimedJoy);
  d->timeout_period_ = std::move(timeout);

  if (isActive())
  {
    // Restarting an active joystick, just bump the start time.
    d->start_time_ = ros::Time::now();
  }
}

ros::Duration GoalTimedJoy::duration() const noexcept
{
  const DS_D(GoalTimedJoy);
  return d->timeout_period_;
}

ros::Duration GoalTimedJoy::remaining() noexcept
{
  if (!isActive())
  {
    return ros::Duration{ 0 };
  }

  DS_D(GoalTimedJoy);

  return duration() - (ros::Time::now() - d->start_time_);
}

void GoalTimedJoy::setUpdatePeriod(ros::Duration period)
{
  if (period == updatePeriod())
  {
    return;
  }

  if (period.toSec() <= 0)
  {
    ROS_ERROR_STREAM("Update period must be > 0");
    return;
  }

  DS_D(GoalTimedJoy);
  d->publish_period_ = std::move(period);

  const auto restart = isActive();
  ROS_DEBUG_STREAM("Stopping publish timer.");
  d->publish_timer_.stop();
  ROS_DEBUG_STREAM("Setting publish timer's period");
  d->publish_timer_.setPeriod(d->publish_period_);

  if (restart)
  {
    ROS_DEBUG_STREAM("Restarting publish timer.");
    d->publish_timer_.start();
  }
}

ros::Duration GoalTimedJoy::updatePeriod() const noexcept
{
  const DS_D(GoalTimedJoy);
  return d->publish_period_;
}

bool GoalTimedJoy::start(const sentry_msgs::TimedJoystick& msg)
{
  DS_D(GoalTimedJoy);
  if (d->publish_period_.toSec() <= 0)
  {
    ROS_ERROR_STREAM("Invalid publish period: " << std::fixed << d->publish_period_.toSec());
    return false;
  }

  if (msg.timeout.toSec() < 0)
  {
    ROS_ERROR_STREAM("Invalid timeout period: " << std::fixed << d->publish_period_.toSec());
    return false;
  }

  if (msg.timeout.isZero())
  {
    d->state_ = msg.state;
    ROS_INFO_STREAM("Stopping joystick due to 0 value timeout duration");
    stop();
    return true;
  }

  d->start_time_ = ros::Time::now();

  // Set ourselves as the active joystick.
  setEnabled(true);

  // Set the controller
  if (msg.controller != sentry_msgs::TimedJoystick::DO_NOT_SET)
  {
    setController(msg.controller);
  }

  // And allocation
  if (msg.allocation != sentry_msgs::TimedJoystick::DO_NOT_SET)
  {
    setAllocation(msg.allocation);
  }

  // Save the desired reference state to send.
  d->state_ = msg.state;

  // Set the duration
  setDuration(msg.timeout);

  ROS_WARN_STREAM("Starting publish timer.");

  // Fire one off immediately.
  publishTimerCallback({});
  d->publish_timer_.start();
  d->is_active_ = true;
  return true;
}

void GoalTimedJoy::stop()
{
  if (!isActive())
  {
    return;
  }

  DS_D(GoalTimedJoy);
  ROS_WARN_STREAM("Stopping publish timer.");
  d->publish_timer_.stop();

  auto msg = sentry_msgs::TimedJoystick{};
  msg.state = d->state_;
  msg.timeout = ros::Duration{ 0 };
  d->timed_joystick_pub_.publish(msg);

  d->is_active_ = false;

  // Clear the state value
  d->state_ = ds_nav_msgs::AggregatedState{};
}

void GoalTimedJoy::setupTimers()
{
  DsProcess::setupTimers();
  DS_D(GoalTimedJoy);
  auto nh = nodeHandle();
  d->publish_timer_ = nh.createTimer(d->publish_period_, &GoalTimedJoy::publishTimerCallback, this, false, false);
}
void GoalTimedJoy::setupPublishers()
{
  JoystickBase::setupPublishers();
  DS_D(GoalTimedJoy);
  auto nh = nodeHandle();
  const auto topic_name = ros::names::resolve(ros::this_node::getName(), std::string{ "timed_joystick_active" });
  d->timed_joystick_pub_ = nh.advertise<sentry_msgs::TimedJoystick>(topic_name, 1, false);
}

void GoalTimedJoy::setupServices()
{
  JoystickBase::setupSubscriptions();

  DS_D(GoalTimedJoy);
  auto nh = nodeHandle();
  d->timed_joystick_srv_ = nh.advertiseService<sentry_msgs::TimedJoystickService::Request, sentry_msgs::TimedJoystickService::Response>(
      ros::names::resolve(ros::this_node::getName(), std::string{ "timed_joystick_command" }),
      boost::bind(&GoalTimedJoyPrivate::startAsService, d, this, _1, _2));
}

void GoalTimedJoy::publishTimerCallback(const ros::TimerEvent&)
{
  ROS_WARN("Inside Publish Callback");
  auto msg = sentry_msgs::TimedJoystick{};
  DS_D(GoalTimedJoy);
  msg.state = d->state_;
  msg.timeout = d->timeout_period_ - (ros::Time::now() - d->start_time_);
  msg.allocation = getAllocation();
  msg.controller = getController();
  if (msg.timeout.toSec() <= 0)
  {
    stop();
    return;
  }

  d->timed_joystick_pub_.publish(msg);
  applyGainsAndPublish(d->state_);
  ROS_WARN_STREAM("Time left: " << msg.timeout.toSec());
}
}
